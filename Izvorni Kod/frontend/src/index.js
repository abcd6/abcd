import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import './index.css';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import green from '@material-ui/core/colors/green';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#006064',
    },
    secondary: {
      main: green[500],
    },
  },
});


ReactDOM.render(
   
     <ThemeProvider theme={theme}>
        <App />   
     </ThemeProvider>,
  document.getElementById('root')
);

