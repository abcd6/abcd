import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import facebookLogo from '../resources/facebook.png';
import instagramLogo from '../resources/instagram.png';

const useStyles = makeStyles((theme) => ({
  title: {
    fontWeight: 'bold',
  },
  a: {
    padding: '5px',
    color: 'white',
  },
  p: {
    padding: '5px',
  },
  img: {
    display: 'flex',
    flexDirection: 'column',
    width: '30px',
    height: '30px',
  },
  imgWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  footerColumn: {
    display: 'flex',
    flexDirection: 'column',
  },
  footerContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  footer: {
    padding: theme.spacing(5, 2),
    marginTop: 'auto',
    backgroundColor: '#616161',
    color: 'white',
  },
}));

export default function Footer() {
  const classes = useStyles();

  return (
    <footer className={classes.footer}>
      <Container className={classes.footerContainer} maxWidth="sm">

        <div className={classes.footerColumn}>
          <Typography className={classes.title} variant="body1">Content</Typography>
          <a className={classes.a} href="/objava">Story</a>
          <a className={classes.a} href="/">Models</a>
        </div>

        <div className={classes.footerColumn}>
          <Typography className={classes.title} variant="body1">Contact us</Typography>
          <div><p>+123 456 789</p><p>+385 911 222</p></div>
        </div>

        <div className={classes.footerColumn}>
          <Typography className={classes.title} variant="body1">Social</Typography>
          <div className={classes.imgWrapper}>
            <a href="/#"><img className={classes.img} src={facebookLogo} alt="FacebookLogo"></img></a>
            <p className={classes.p}>Facebook</p>
          </div>
          <div className={classes.imgWrapper}>
            <a href="/#"><img className={classes.img} src={instagramLogo} alt="InstagramLogo"></img></a>
            <p className={classes.p}>Instagram</p>
          </div>
        </div>

      </Container>
    </footer>
  );
}