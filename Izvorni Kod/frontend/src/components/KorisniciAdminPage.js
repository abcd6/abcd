import React, { Component } from 'react'
import { Grid } from '@material-ui/core'
import Axios from 'axios';
import KorisniciAdminDetail from './KorisniciAdminDetail';
import * as Const from './Constants';

export default class KorisniciAdminPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            osobniPodaci: null
        }
        this.getOsobniPodaci = this.getOsobniPodaci.bind(this);
        this.updateOsobniPodaci = this.updateOsobniPodaci.bind(this);
    }

    getOsobniPodaci() {
        Axios.get('/api/osobniPodaciAdmin')
            .then((response) => {
                if (response.data.osobniPodaci != null) {
                    this.setState({
                        osobniPodaci: response.data.osobniPodaci
                    })
                }
            })
            .catch(function (error) {
                console.log(error);
                alert(error.response.status!==500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
            });
    }

    componentDidMount() {
        this.getOsobniPodaci();
    }

    updateOsobniPodaci(props) {
        if (props != null) {
            this.setState({
                osobniPodaci: props
            })
        }
    }

    render() {
        return (
            <Grid container>
                {this.state.osobniPodaci != null ?
                    this.state.osobniPodaci.map((osobniPodaciProp, index) =>
                        <KorisniciAdminDetail
                            key={index}
                            osobniPodaciProp={osobniPodaciProp}
                            updateOsobniPodaciProp={this.updateOsobniPodaci}
                        />) : null}
            </Grid>
        );
    }
}
