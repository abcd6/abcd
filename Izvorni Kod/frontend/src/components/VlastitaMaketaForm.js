import React from 'react'
import { Button, Box, Grid, makeStyles, TextField, Typography, Dialog, FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';
import Axios from 'axios';
import AuthenticationService from './AuthenticationService';
import InputAdornment from '@material-ui/core/InputAdornment';
import * as Const from './Constants';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        margin: theme.spacing(0, 3, 0),
        borderRadius: 20,
        padding: theme.spacing(2, 2)
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    form: {
        width: '100%', // IE 11
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
        backgroundColor: '#dfd3d3',
    },
    button: {
        margin: theme.spacing(3, 0, 2),
        backgroundColor: '#dfd3d3',
    }
}));

let maketa = {
    naziv: "",
    opis: "",
    visina: "",
    širina: "",
    dužina: "",
    mjerilo: "",
    boja: "",
    cijena: "",
    standardna: "",
    slika: ""
}

export default function VlastitaMaketaForm(props) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [materijalID, setmaterijalID] = React.useState(null);
    const [materijalList, setmaterijalList] = React.useState(null);

    if (materijalList === null) {
        Axios.get('/api/materijali')
            .then(function (response) {
                setmaterijalList(response.data.materijal);
            })
            .catch(function (error) {
                console.log(error);
                alert(error.response.status !== 500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
            });
    }

    const handleChange = (e) => {
        setmaterijalID(e.target.value);
    };

    const handleSubmit = (e) => {
        e.preventDefault()


        const id = AuthenticationService.getLoggedInUserId();
        Axios.get('/api/getOsobniPodaciById', {
            params: {
                id
            }
        }).then(function (response) {
            let narudzba = {
                osobniPodaciId: response.data.id,
                maketa: maketa,
                materijalId: materijalID
            }


            maketa.naziv = e.target.naziv.value;
            maketa.opis = e.target.opis.value;
            maketa.visina = e.target.visina.value;
            maketa.širina = e.target.širina.value;
            maketa.dužina = e.target.dužina.value;
            maketa.mjerilo = e.target.mjerilo.value;
            maketa.boja = e.target.boja.value;
            maketa.standardna = false;

            Axios.post('/api/novaNarudzba',
                narudzba
            )
                .then(function (response) {
                    props.updateMaketaListProp(response.data);
                    maketa = {
                        naziv: "",
                        opis: "",
                        visina: "",
                        širina: "",
                        dužina: "",
                        mjerilo: "",
                        boja: "",
                        cijena: "",
                        standardna: false,
                        slika: ""
                    }
                    
                    setOpen(false)
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error.response.status !== 500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
                });
        })
            .catch(function (error) {
                console.log(error);
                alert(error.response.status !== 500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
            });
    }

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const getBase64 = (file, cb) => {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            cb(reader.result)
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }
    const onFileChange = (e) => {
        e.preventDefault();
        getBase64(e.target.files[0], (result) => {
            maketa.slika = result;
        });
    }

    return (
        <div className={classes.root}>       
            <Button variant="outlined" color="primary" onClick={handleClickOpen}>
                Naruči vlastitu maketu
            </Button>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <div className={classes.paper}>
                    <Typography component="h1" variant="h5">
                        Naruči vlastitu maketu
                    </Typography>
                    <form className={classes.form} onSubmit={handleSubmit}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    autoFocus
                                    required
                                    fullWidth
                                    id="naziv"
                                    name="naziv"
                                    label="Naziv"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    id="opis"
                                    name="opis"
                                    label="Opis"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    id="visina"
                                    name="visina"
                                    label="Visina"
                                    InputProps={{
                                        endAdornment: <InputAdornment position="end">cm</InputAdornment>,
                                    }}
                                />
                            </Grid>

                            <Grid item xs={12} sm={6}>
                                <TextField
                                    required
                                    fullWidth
                                    id="širina"
                                    name="širina"
                                    label="Širina"
                                    InputProps={{
                                        endAdornment: <InputAdornment position="end">cm</InputAdornment>,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    required
                                    fullWidth
                                    id="dužina"
                                    name="dužina"
                                    label="Dužina"
                                    InputProps={{
                                        endAdornment: <InputAdornment position="end">cm</InputAdornment>,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    id="mjerilo"
                                    name="mjerilo"
                                    label="Mjerilo"
                                />
                            </Grid>
                            <FormControl variant="outlined" fullWidth className={classes.drop}>
                                <InputLabel id="demo-simple-select-outlined-label">Materijal</InputLabel>
                                <Select
                                    labelId="demo-simple-select-outlined-label"
                                    id="demo-simple-select-outlined"
                                    value={materijalID ? materijalID : ""}
                                    onChange={handleChange}
                                    label="Materijal"
                                >{materijalList != null ?
                                    materijalList.map((materijalProp, index) =>
                                        <MenuItem key={index} value={materijalProp.id}>{materijalProp.naziv} - {materijalProp.cijena} kn / cm³</MenuItem>
                                    ) : null}
                                </Select>
                            </FormControl>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    id="boja"
                                    name="boja"
                                    label="Boja"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <label>Dodaj sliku</label>
                            </Grid>
                            <Grid item xs={12}>
                                <input id="slika" type="file" accept="image/png, image/jpeg" onChange={onFileChange} />
                            </Grid>
                        </Grid>
                        <Button
                            type="submit"
                            variant="contained"
                            className={classes.submit}
                        >
                            <Box fontWeight="fontWeightBold" m={1}>
                                Naruči maketu
                            </Box>
                        </Button>
                        &nbsp;
                        <Button
                            type="submit"
                            variant="contained"
                            className={classes.button}
                            onClick={handleClose}
                        >
                            <Box fontWeight="fontWeightBold" m={1} onClick={handleClose}>
                                Odustani
                                    </Box>
                        </Button>
                    </form>
                </div>
            </Dialog>
        </div>
    );
}
