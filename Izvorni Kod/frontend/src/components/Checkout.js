import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import AddressForm from './AddressForm';
import PaymentForm from './PaymentForm';
import Review from './Review';
import * as Const from './Constants';
import { useLocation } from 'react-router-dom';
import axios from 'axios';
import AuthenticationService from './AuthenticationService'

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright ©MaketaShop '}

      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: 'relative',
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      width: 600,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3),
    },
  },
  stepper: {
    padding: theme.spacing(3, 0, 5),
  },
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1),
  },
}));

const steps = ['Adresa pošiljke', 'Detalji plaćanja', 'Pregled narudžbe'];

function getStepContent(steps) {
  switch (steps) {
    case 0:
      return <AddressForm korisnikProp={korisnik} />;
    case 1:
      return <PaymentForm karticaProp={kartica} />;
    case 2:
      return <Review maketa={maketa} korisnik={korisnik} kartica={kartica} materijal={materijal} />;
    default:
      throw new Error('Unknown step');
  }
}

let maketa = {
  naziv: "",
  cijena: "",
}

let materijal = {
  naziv: "",
}

let korisnik = {
  ime: "", prezime: "", adresa: "", drzava: "", postanskiBroj: ""
}

let kartica = {
  imeKartice: "", brojKartice: "", datumIstek: "", cvv: ""
}

export default function Checkout() {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);

  let location = useLocation();
  const id = location.state.maketaId;
  const materijalID = location.state.materijalID;

  axios.get('/api/getMaketaById', {
    params: {
      id
    }
  }).then(function (response) {
    maketa.naziv = response.data.naziv
    maketa.cijena = location.state.cijena
  })
    .catch(function (error) {
      console.log(error);
      alert(error.response.status!==500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
    });

  axios.get('/api/getMaterijalById', {
    params: {
      materijalID
    }
  }).then(function (response) {
    materijal.naziv = response.data.naziv
  })
    .catch(function (error) {
      console.log(error);
      alert(error.response.data ? error.response.data : Const.SERVER_CONNECTION_ERROR);
    });

  if (activeStep === 0 && AuthenticationService.isUserLoggedIn()) {

    const id = AuthenticationService.getLoggedInUserId();

    axios.get('/api/getOsobniPodaciById', {
      params: {
        id
      }
    }).then(function (response) {
      setActiveStep(1);
      korisnik.ime = response.data.ime;
      korisnik.prezime = response.data.prezime;
      korisnik.adresa = response.data.adresa;
      korisnik.postanskiBroj = response.data.poštanskibroj;
      korisnik.drzava = response.data.država;
    })
      .catch(function (error) {
        console.log(error);
        alert(error.response.status!==500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
      });
  }
  let iWantToMove = true;

  const handleSubmit = (e) => {
    e.preventDefault();
    if (activeStep === 0) {
      korisnik.ime = e.target.firstName.value;
      korisnik.prezime = e.target.lastName.value;
      korisnik.adresa = e.target.address1.value;
      korisnik.postanskiBroj = e.target.zip.value;
      korisnik.drzava = e.target.country.value;
    }
    else if (activeStep === 1) {
      if (e.target.cvv.value.length === 3) {
        kartica.imeKartice = e.target.cardName.value
        kartica.brojKartice = e.target.cardNumber.value;
        kartica.datumIstek = e.target.expDate.value;
        kartica.cvv = e.target.cvv.value;
        iWantToMove = true;
      } else {
        iWantToMove = false;
        alert("Za CVV potrebno je upisati točno 3 znamenke")
      }
    }

    else if (activeStep === steps.length - 1) {
      axios.post('/api/transakcije', {
        korisnikID: AuthenticationService.getLoggedInUserId(),
        maketaID: location.state.maketaId,
        materijalID: materijalID,
        cijena: maketa.cijena
      })
        .then(function (response) {
          maketa = {
            naziv: "",
            cijena: "",
          }

          korisnik = {
            ime: "", prezime: "", adresa: "", drzava: "", postanskiBroj: ""
          }

          kartica = {
            imeKartice: "", brojKartice: "", datumIstek: "", cvv: ""
          }
        })
        .catch(function (error) {
          console.log(error);
          alert(error.response.status!==500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
        });
    }
    if (iWantToMove) {
      setActiveStep(activeStep + 1);
    }
  };

  const handleBack = () => {
    setActiveStep(activeStep - 1);
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <main className={classes.layout}>
        <Paper className={classes.paper}>
          <form onSubmit={handleSubmit}>
            <Typography component="h1" variant="h4" align="center">
              Plaćanje narudžbe
            </Typography>
            <Stepper activeStep={activeStep} className={classes.stepper}>
              {steps.map((label) => (
                <Step key={label}>
                  <StepLabel>{label}</StepLabel>
                </Step>
              ))}
            </Stepper>
            <React.Fragment>
              {activeStep === steps.length ? (
                <React.Fragment>
                  <Typography variant="h5" gutterBottom>
                    Hvala na Vašoj narudžbi.
                </Typography>
                </React.Fragment>
              ) : (
                  <React.Fragment>
                    {getStepContent(activeStep, location.state.maketaId)}
                    <div className={classes.buttons}>
                      {((activeStep > 1 && (AuthenticationService.isUserLoggedIn())) || (activeStep > 0 && !(AuthenticationService.isUserLoggedIn()))) && (
                        <Button
                          variant='contained'
                          color='primary'
                          onClick={handleBack}
                          className={classes.button}>
                          {'Prethodni korak'}
                        </Button>
                      )}
                      <Button
                        variant="contained"
                        color="primary"
                        type="submit"
                        className={classes.button}
                      >
                        {activeStep === steps.length - 1 ? 'Završi plaćanje' : 'Sljedeći korak'}

                      </Button>
                    </div>
                  </React.Fragment>
                )}
            </React.Fragment>
          </form>
        </Paper>
        <Copyright />
      </main>
    </React.Fragment>
  );
}