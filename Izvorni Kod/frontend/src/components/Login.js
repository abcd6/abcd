import React from 'react'
import { Avatar, Button, Box, CssBaseline, Grid, makeStyles, Link, TextField, Typography, FormControlLabel, Checkbox, Paper } from '@material-ui/core'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import * as Const from './Constants'
import PozSlika from "../resources/loginSlika.jpg"
import AuthenticationService from './AuthenticationService';
import { useHistory } from "react-router";
import { Link as RouterLink } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh',
    },
    image: {
        backgroundImage: `url(${PozSlika})`,
        backgroundRepeat: 'no-repeat',
        backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: "#7c7575",
    },
    form: {
        width: '100%', // IE 11
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
        backgroundColor: "#dfd3d3",
    },
}));



export default function Login(props) {
    const classes = useStyles();
    const history = useHistory();

    const handleSubmit = (e) => {
        e.preventDefault();

        AuthenticationService
            .executeJwtAuthenticationService(e.target.username.value, e.target.password.value)
            .then((response) => {
                AuthenticationService.registerSuccessfulLoginForJwt(response.data.token, e.target.remember.checked)
                props.setuserLoggedIn(true);
                history.push("/")
            }).catch((error) => {
                console.log(error);
            })
    }

    return (
        <Grid container component="main" className={classes.root}>
            <CssBaseline />

            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h4">
                        {Const.LOGIN}
                    </Typography>
                    <form className={classes.form} onSubmit={handleSubmit}>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="username"
                            label={Const.USERNAME}
                            name="username"
                            autoComplete="username"
                            autoFocus
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="password"
                            label={Const.PASSWORD}
                            name="password"
                            type="password"
                            autoComplete="current-password"
                        />
                        <FormControlLabel
                            control={<Checkbox name="remember" color="primary" />}
                            label={Const.REMEMBER}
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            className={classes.submit}
                        >
                            <Box fontWeight="fontWeightBold" m={1}>
                                {Const.LOGIN}
                            </Box>
                        </Button>
                        <Grid container>
                            <Grid item>
                                <Link component={RouterLink} to="/register" variant="body2">
                                    {"Nemate korisnički račun? Registrirajte se."}
                                </Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Grid>
            <Grid item xs={false} sm={4} md={7} className={classes.image} />
        </Grid>

    )
}
