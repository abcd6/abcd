import axios from 'axios'
import jwt_decode from "jwt-decode";
import * as Const from './Constants';

export const USER_ID = 'USER_ID'
export const USER_NAME = 'USER_NAME'
export const USER_ROLE = 'USER_ROLE'
export const USER_TOKEN = 'USER_TOKEN'
export const USER_STORAGE = 'USER_STORAGE'

let tokenStorage = sessionStorage;

class AuthenticationService {

    executeJwtAuthenticationService(username, password) {
        return axios.post("api/login", {
            username: username,
            password: password
        }).catch(function (error) {
            console.log(error);
            alert(error.response.status !== 500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
        });
    }

    registerSuccessfulLoginForJwt(tokenParam, rememberMe) {
        if (tokenParam !== null) {
            var decodedToken = jwt_decode(tokenParam);
            if (rememberMe === true) {
                tokenStorage = localStorage;
            }
            tokenStorage.setItem(USER_TOKEN, tokenParam);
            tokenStorage.setItem(USER_STORAGE, rememberMe);
            tokenStorage.setItem(USER_ID, decodedToken.jti);
            tokenStorage.setItem(USER_NAME, decodedToken.sub);
            tokenStorage.setItem(USER_ROLE, decodedToken.aud);
            this.setupAxiosInterceptors(this.createJWTToken(tokenParam))
        } else if (this.isUserLoggedIn()) {
            this.setupAxiosInterceptors(this.createJWTToken(tokenStorage.getItem(USER_TOKEN)))
        } else {
            this.logout()
        }


    }

    createJWTToken(token) {
        return 'Bearer ' + token
    }


    logout() {
        tokenStorage.removeItem(USER_ID);
        tokenStorage.removeItem(USER_TOKEN);
        tokenStorage.removeItem(USER_STORAGE);
        tokenStorage.removeItem(USER_NAME);
        tokenStorage.removeItem(USER_ROLE);
        window.location.href = "/";
    }

    isUserLoggedIn() {
        let id = tokenStorage.getItem(USER_ID)
        let user = tokenStorage.getItem(USER_NAME)
        let role = tokenStorage.getItem(USER_ROLE)
        let token = tokenStorage.getItem(USER_TOKEN)
        let storage = tokenStorage.getItem(USER_STORAGE)
        if (id === null || user === null || role === null || token === null || storage === null) return false
        return true
    }

    getLoggedInUserId() {
        let id = tokenStorage.getItem(USER_ID)
        if (id === null) return '2'
        return id
    }

    getLoggedInUserName() {
        let user = tokenStorage.getItem(USER_NAME)
        if (user === null) return 'anon'
        return user
    }

    getLoggedInUserRole() {
        let role = tokenStorage.getItem(USER_ROLE)
        if (role === null) return 'ROLE_ANON'
        return role
    }

    getLoggedInUserToken() {
        let token = tokenStorage.getItem(USER_TOKEN)
        if (token === null) return ''
        return token
    }

    setupAxiosInterceptors(token) {

        axios.interceptors.request.use(
            (config) => {
                if (this.isUserLoggedIn()) {
                    config.headers.authorization = token
                }
                return config
            }
        )
    }
}

export default new AuthenticationService()