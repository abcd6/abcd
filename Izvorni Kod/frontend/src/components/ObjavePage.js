import React, { Component } from 'react'
import { Grid } from '@material-ui/core'
import Axios from 'axios';
import ObjavaElement from './ObjavaElement';
import AuthenticationService from './AuthenticationService';
import ObjavaForm from './ObjavaForm';
import * as Const from './Constants';

export default class ObjavePage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            objavaList: null
        }
        this.getObjavaList = this.getObjavaList.bind(this);
        this.updateObjavaList = this.updateObjavaList.bind(this);
    }

    getObjavaList() {
        Axios.get('/api/objava')
            .then((response) => {
                if (response.data.objava != null) {
                    this.setState({
                        objavaList: response.data.objava
                    })
                }
            })
            .catch(function (error) {
                console.log(error);
                alert(error.response.status!==500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
            });
    }

    updateObjavaList(props) {
        if (props != null) {
            this.setState({
                objavaList: props
            })
        }
    }

    componentDidMount() {
        this.getObjavaList();
    }

    render() {
        return (
            <Grid style={{ padding: '35px' }} container spacing={3}>
                {AuthenticationService.isUserLoggedIn() ?
                    <ObjavaForm updateObjavaListProp={this.updateObjavaList} />
                    : null
                }
                {this.state.objavaList != null ?
                    this.state.objavaList.map((objavaProp, index) => <React.Fragment key={index}>{
                        objavaProp.odobreno || AuthenticationService.getLoggedInUserRole() === 'ROLE_ADMIN' ?

                            <ObjavaElement objavaProp={objavaProp} updateObjavaList={this.updateObjavaList} />
                            : null}</React.Fragment>) : null}
            </Grid>
        );
    }
}
