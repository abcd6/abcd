import React, { Component } from 'react'
import { Grid } from '@material-ui/core'
import Axios from 'axios';
import OsobniPodaciDetail from './OsobniPodaciDetail';
import AuthenticationService from './AuthenticationService';
import * as Const from './Constants';

export default class OsobniPodaciPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            osobniPodaci: null
        }
        this.getOsobniPodaci = this.getOsobniPodaci.bind(this);
        this.updateOsobniPodaci = this.updateOsobniPodaci.bind(this);
    }

    getOsobniPodaci() {
        const username = AuthenticationService.getLoggedInUserName();
        if (username !== "anon") {
            Axios.get('/api/getOsobniPodaciByUsername', {
                params: {
                    username
                }
            })
                .then((response) => {
                    if (response.data != null) {
                        this.setState({
                            osobniPodaci: response.data
                        })
                    }
                })
                .catch(function (error) {
                    console.log(error);
                    alert(error.response.status !== 500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
                });
        }
    }

    updateOsobniPodaci(props) {
        if (props != null) {
            this.setState({
                osobniPodaci: props
            })
        }
    }


    componentDidMount() {
        this.getOsobniPodaci();
    }


    render() {
        return (
            <Grid container>
                {this.state.osobniPodaci != null ?
                    <OsobniPodaciDetail
                        osobniPodaciProp={this.state.osobniPodaci}
                        updateOsobniPodaciProp={this.updateOsobniPodaci}
                    /> : null}
            </Grid>
        );
    }
}
