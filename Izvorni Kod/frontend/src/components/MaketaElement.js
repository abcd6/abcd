import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import noImage from "../resources/nemaslike.jpg"
import { Button } from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import { NavLink } from "react-router-dom";
import Divider from '@material-ui/core/Divider';


const useStyles = makeStyles((theme) => ({
    root: {
        background: '#eeecee',
        marginTop: 60,
        minWidth: 400,
        margin: theme.spacing(2, 1),
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    sections: {
        margin: theme.spacing(2),
    },
    section: {
        margin: theme.spacing(2, 2),
        maxWidth: 290
    },
    drop: {
        width: '100%',
        maxWidth: 290,
    }
}));

export default function MaketaElement(props) {
    const classes = useStyles();

    const [materijalID, setmaterijalID] = React.useState(null);
    const [cijena, setCijena] = React.useState(null);

    const handleChange = (e) => {
        let i;
        for (i = 0; i < props.maketaProp.materijalList.length; i++) {
            if (e.target.value === props.maketaProp.materijalList[i].id) {
                let volumen = props.maketaProp.maketa.širina * props.maketaProp.maketa.visina * props.maketaProp.maketa.dužina;
                document.getElementById(props.maketaProp.maketa.id).textContent = (props.maketaProp.maketa.cijena + volumen * props.maketaProp.materijalList[i].cijena).toFixed(2);
                setCijena(document.getElementById(props.maketaProp.maketa.id).innerHTML)
            }
        }
        setmaterijalID(e.target.value);
    };

    const [expanded, setExpanded] = React.useState(false);

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };

    return (
        <Card className={classes.root}>
            <CardHeader
                title={props.maketaProp.maketa.naziv}
                subheader={"Cijena " + props.maketaProp.maketa.cijena + " kn"}
            />
            <CardContent>
                {props.maketaProp.maketa.slika ?
                    <CardMedia
                        className={classes.media}
                        image={props.maketaProp.maketa.slika}
                        alt={props.maketaProp.maketa.naslov}
                    /> :
                    <CardMedia
                        className={classes.media}
                        image={noImage}
                        alt={props.maketaProp.maketa.naslov}
                    />
                }
            </CardContent>
            <CardActions disableSpacing>
                <Typography style={{ paddingLeft: '10px' }} variant="body2" color="textSecondary" component="p">
                    Detalji
                </Typography>
                <IconButton
                    className={clsx(classes.expand, {
                        [classes.expandOpen]: expanded,
                    })}
                    onClick={handleExpandClick}
                    aria-expanded={expanded}
                    aria-label="show more"
                >
                    <ExpandMoreIcon />
                </IconButton>
            </CardActions>
            <Collapse in={expanded} timeout="auto" unmountOnExit>
                <CardContent>
                    <div className={classes.sections}>
                        <div className={classes.section}>
                            <Typography gutterBottom variant="h6">
                                Opis:
                            </Typography>
                            <Typography gutterBottom>
                                {props.maketaProp.maketa.opis}
                            </Typography>
                        </div>
                        <div className={classes.section}>
                            <Typography gutterBottom variant="h6">
                                Dimenzije:
                            </Typography>
                            <Typography gutterBottom>
                                {props.maketaProp.maketa.dužina} x {props.maketaProp.maketa.visina} x {props.maketaProp.maketa.širina}
                            </Typography>
                        </div>
                        <Divider variant="middle" />
                        <div className={classes.section}>
                            <Typography gutterBottom variant="h6">
                                Odaberi materijal:
                            </Typography>
                            <div>
                                <FormControl variant="outlined" className={classes.drop}>
                                    <InputLabel id="demo-simple-select-outlined-label">Materijal</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-outlined-label"
                                        id="demo-simple-select-outlined"
                                        value={materijalID ? materijalID : ""}
                                        onChange={handleChange}
                                        label="Materijal"
                                    >{props.maketaProp.materijalList != null ?
                                        props.maketaProp.materijalList.map((materijalProp, index) =>
                                            <MenuItem key={index} value={materijalProp.id}>{materijalProp.naziv}</MenuItem>
                                        ) : null}
                                    </Select>
                                </FormControl>
                            </div>
                        </div>
                        <Divider variant="middle" />
                        <div className={classes.section}>
                            <Typography style={{ display: "inline" }}>Ukupna cijena: <span id={props.maketaProp.maketa.id}>{props.maketaProp.maketa.cijena}</span> kn</Typography>
                            <Button
                                variant='contained'
                                color='primary'
                                disabled={materijalID ? false : true}
                                component={NavLink} to={{ pathname: "/placanje", state: { maketaId: props.maketaProp.maketa.id, materijalID: materijalID, cijena: cijena } }}
                                className={classes.button}
                                style={{ float: 'right', marginBottom: '30px' }}>
                                KUPI
                            </Button>
                        </div>
                    </div>
                </CardContent>
            </Collapse>
        </Card>
    )
}
