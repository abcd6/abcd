import React, { Component } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { Typography } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import Axios from "axios";
import NarudzbaElement from "./NarudzbaElement";
import AuthenticationService from "./AuthenticationService";
import * as Const from './Constants';

export default class NarudzbaList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      narudzbaList: null,
    };
    this.getNarudzbaList = this.getNarudzbaList.bind(this);
    this.updateNarudzbaList = this.updateNarudzbaList.bind(this);
  }

  getNarudzbaList() {
    if (AuthenticationService.getLoggedInUserRole() === "ROLE_ADMIN") {
      Axios.get("/api/narudzbe")
        .then((response) => {
          if (response.data.narudzba != null) {
            this.setState({
              narudzbaList: response.data.narudzba,
            });
          }
        })
        .catch(function (error) {
          console.log(error);
          alert(error.response.status!==500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
        });
    } else if(AuthenticationService.getLoggedInUserRole() === "ROLE_USER"){
      let id = AuthenticationService.getLoggedInUserId();
      Axios.get("/api/narudzbeKorisnik", {
        params: {
          id,
        },
      })
        .then((response) => {
          if (response.data.narudzba != null) {
            this.setState({
              narudzbaList: response.data.narudzba,
            });
          }
        })
        .catch(function (error) {
          console.log(error);
          alert(error.response.status!==500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
        });
    }
  }

  updateNarudzbaList(props) {
    if (props != null) {
      this.setState({
        narudzbaList: props,
      });
    }
  }

  componentDidMount() {
    this.getNarudzbaList();
  }
  render() {
    return (
      <React.Fragment>
        <Typography style={{ marginTop: "60px" }} variant="h4">Narudžbe</Typography>
        <TableContainer component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>ID</TableCell>
                <TableCell>Naziv makete</TableCell>
                <TableCell>Materijal makete</TableCell>
                <TableCell>Datum kreiranja</TableCell>
                <TableCell>Status</TableCell>
                <TableCell>Predložena cijena</TableCell>
                <TableCell>Cijena</TableCell>
                <TableCell></TableCell>
                <TableCell>Obriši</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.narudzbaList != null ?
                this.state.narudzbaList.map((narudzbaProp, index) =>
                  <NarudzbaElement key={index} narudzbaProp={narudzbaProp} updateNarudzbaList={this.updateNarudzbaList} />) : null}
            </TableBody>
          </Table>
        </TableContainer>
      </React.Fragment>
    )
  }
}


