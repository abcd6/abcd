import React, { Component } from "react";
import { Typography } from "@material-ui/core";
import Axios from "axios";
import TransakcijeElement from "./TransakcijeElement";
import AuthenticationService from "./AuthenticationService";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import * as Const from './Constants';

export default class TransakcijePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      transakcijeList: null
    };
    this.getTransakcijaList = this.getTransakcijeList.bind(this);
  }

  getTransakcijeList() {
    if (AuthenticationService.getLoggedInUserRole() === "ROLE_ADMIN") {
      Axios.get("/api/transakcije")
        .then((response) => {
          if (response.data.transakcija != null) {
            this.setState({
              transakcijeList: response.data.transakcija,
            });
          }
        })
        .catch(function (error) {
          console.log(error);
          alert(error.response.status!==500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
        });
    } else if (AuthenticationService.getLoggedInUserRole() === "ROLE_USER") {
      let id = AuthenticationService.getLoggedInUserId();
      Axios.get("/api/transakcijeKorisnik", {
        params: {
          id,
        },
      })
        .then((response) => {
          if (response.data.transakcija != null) {
            this.setState({
              transakcijeList: response.data.transakcija,
            });
          }
        })
        .catch(function (error) {
          console.log(error);
          alert(error.response.status!==500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
        });
    }
  }

  componentDidMount() {
    this.getTransakcijeList();
  }

  render() {
    return (
      <React.Fragment>
        <Typography style={{ marginTop: "60px" }} variant="h4">Transakcije</Typography>
        <TableContainer component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>ID</TableCell>
                <TableCell>Naziv makete</TableCell>
                <TableCell>Kupac</TableCell>
                <TableCell>Materijal</TableCell>
                <TableCell>Cijena</TableCell>
                <TableCell>Datum transakcije</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.transakcijeList != null
                ? this.state.transakcijeList.map((transakcijaProp, index) => (
                  <TransakcijeElement
                    key={index}
                    transakcijaProp={transakcijaProp}
                  />
                ))
                : null}
            </TableBody>
          </Table>
        </TableContainer>
      </React.Fragment>
    );
  }
}
