import React, { useState } from 'react'
import { Avatar, Button, Box, Checkbox, CssBaseline, FormControlLabel, Grid, Link, makeStyles, TextField, Typography, Paper } from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import * as Const from './Constants'
import PozSlika from "../resources/loginSlika.jpg"

import { Link as RouterLink } from "react-router-dom";

import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { hr } from 'date-fns/locale';

import Axios from 'axios';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  image: {
    backgroundImage: `url(${PozSlika})`,
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: "#7c7575",
  },
  form: {
    width: '100%', // IE 11
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: '#dfd3d3',
  },
}));



const handleSubmit = (e) => {
  e.preventDefault();
  let korisnik = {
    username: e.target.username.value
  }
  let osobniPodaci = {
    ime: e.target.firstName.value,
    prezime: e.target.lastName.value,
    datumrođenja: new Date(e.target.birthday.value),
    mobitel: e.target.phone.value,
    email: e.target.email.value,
    adresa: e.target.address.value,
    država: e.target.country.value,
    poštanskibroj: e.target.postalcode.valueAsNumber,
    privatni: e.target.private.checked
  }
  let password=e.target.password.value;

  Axios.post('/api/register', {
    korisnik,
    osobniPodaci,
    password
  })
    .then(function (response) {
      alert("Registracija uspješna");
      window.location.href = "/login";
    })
    .catch(function (error) {
      console.log(error);
      alert(error.response.status!==500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
    });
}

export default function Register() {
  const classes = useStyles();
  const [selectedDate, handleDateChange] = useState(new Date());

  return (
    <Grid container component="main">
      <CssBaseline />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            {Const.REGISTER}
          </Typography>
          <form className={classes.form} onSubmit={handleSubmit}>
            <Grid container spacing={2}>

              <Grid item xs={12}>
                <TextField
                  autoFocus
                  required
                  fullWidth
                  id="username"
                  name="username"
                  label={Const.USERNAME}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="password"
                  name="password"
                  label={Const.PASSWORD}
                  type="password"
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  fullWidth
                  id="firstName"
                  name="firstName"
                  label={Const.NAME}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  fullWidth
                  id="lastName"
                  name="lastName"
                  label={Const.SURNAME}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="email"
                  name="email"
                  label={Const.EMAIL}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="phone"
                  name="phone"
                  label={Const.PHONE}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="country"
                  name="country"
                  label={Const.COUNTRY}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="postalcode"
                  name="postalcode"
                  label={Const.POSTALCODE}
                  type="number"
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="address"
                  name="address"
                  label={Const.ADDRESS}
                />
              </Grid>
              <Grid item xs={12}>
                <MuiPickersUtilsProvider utils={DateFnsUtils} locale={hr} >
                  <Grid item xs={12}>
                    <KeyboardDatePicker
                      disableToolbar
                      fullWidth
                      variant="inline"
                      format="dd/MM/yyyy"
                      id="birthday"
                      name="birthday"
                      label={Const.BIRTHDAY}
                      value={selectedDate}
                      onChange={handleDateChange}
                    />
                  </Grid>
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item xs={12}>
                <FormControlLabel
                  control={<Checkbox id="private" name="private" color="primary" />}
                  label={Const.PRIVATNI}
                />
              </Grid>

            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              className={classes.submit}
            >
              <Box fontWeight="fontWeightBold" m={1}>
                {Const.REGISTER}
              </Box>
            </Button>
            <Grid container justify="flex-end">
              <Grid item>
                <Link component={RouterLink} to="/login" variant="body2">
                  {Const.ALREADYUSER}
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
      </Grid>
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
    </Grid>
  );
}