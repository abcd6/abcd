import React from 'react';
import { Button, Box, Grid, makeStyles, TextField, Typography, Dialog } from '@material-ui/core';
import Axios from 'axios';
import AuthenticationService from './AuthenticationService';
import * as Const from './Constants';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        margin: theme.spacing(0, 3, 0),
        borderRadius: 20,
        padding: theme.spacing(2, 2)
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    form: {
        width: '100%', // IE 11
        marginTop: theme.spacing(3),
    },
    button: {
        margin: theme.spacing(3, 0, 2),
        backgroundColor: '#dfd3d3',
    }
}));

let objava = {
    naslov: "",
    tekst: "",
    slika: "",
    video: "",
    korisnikID: ""
}

export default function ObjavaForm(props) {

    const classes = useStyles();

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleSubmit = (e) => {
        e.preventDefault()

        objava.naslov = e.target.naslov.value;
        objava.tekst = e.target.tekst.value;
        objava.korisnikID = AuthenticationService.getLoggedInUserId();

        console.log(objava)

        Axios.post('/api/objava', objava)
            .then(function (response) {
                props.updateObjavaListProp(response.data.objava);
                objava = {
                    naslov: "",
                    tekst: "",
                    slika: "",
                    video: "",
                    korisnikID: AuthenticationService.getLoggedInUserId()
                }
                
                setOpen(false)
            })
            .catch(function (error) {
                console.log(error);
                alert(error.response.status!==500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
            });
    }

    const getBase64 = (file, cb) => {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            cb(reader.result)
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }

    const onFileChange = (e) => {
        getBase64(e.target.files[0], (result) => {
            objava.slika = result;
        });
    }

    const onFileChangeVideo = (e) => {
        getBase64(e.target.files[0], (result) => {
            objava.video = result;
        });
    }

    /**********************/

    return (
        <div className={classes.root}>
            {AuthenticationService.getLoggedInUserRole() === 'ROLE_ADMIN' ?
                <Button variant="outlined" color="primary" onClick={handleClickOpen}>
                    Dodaj objavu
                </Button>
                :
                <Button variant="outlined" color="primary" onClick={handleClickOpen}>
                    Predloži objavu
                </Button>
            }
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <div className={classes.paper}>
                    {AuthenticationService.getLoggedInUserRole() === 'ROLE_ADMIN' ?
                        <Typography component="h1" variant="h5">
                            Dodaj objave
                        </Typography>
                        :
                        <Typography component="h1" variant="h5">
                            Predlaganje objave
                        </Typography>
                    }
                    <form className={classes.form} onSubmit={handleSubmit}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    id="naslov"
                                    name="naslov"
                                    label="naslov"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    id="tekst"
                                    name="tekst"
                                    label="tekst"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <label>Dodaj sliku</label>
                            </Grid>
                            <Grid item xs={12}>
                                <input id="slika" type="file" accept="image/png, image/jpeg" onChange={onFileChange} />
                            </Grid>
                            <Grid item xs={12}>
                                <label>Dodaj video</label>
                            </Grid>
                            <Grid item xs={12}>
                                <input id="video" type="file" accept="video/mp4" onChange={onFileChangeVideo} />
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item>
                                <Button
                                    type="submit"
                                    variant="contained"
                                    className={classes.button}
                                >
                                    <Box fontWeight="fontWeightBold" m={1}>
                                        Predloži objavu
                                    </Box>
                                </Button>
                            </Grid>
                            <Grid item>
                                <Button
                                    variant="contained"
                                    className={classes.button}
                                    onClick={handleClose}
                                >
                                    <Box fontWeight="fontWeightBold" m={1}>
                                        Odustani
                                    </Box>
                                </Button>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Dialog>
        </div>
    );
}
