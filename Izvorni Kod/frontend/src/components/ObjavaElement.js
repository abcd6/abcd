import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Alert from '@material-ui/lab/Alert';
import Axios from 'axios';
import './ObjavaElement';
import { MobileStepper, Paper, useTheme } from '@material-ui/core';
import { KeyboardArrowLeft, KeyboardArrowRight } from '@material-ui/icons';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import CommentIcon from '@material-ui/icons/Comment';
import AuthenticationService from './AuthenticationService';
import DeleteIcon from '@material-ui/icons/Delete';
import Tooltip from '@material-ui/core/Tooltip';
import * as Const from './Constants';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        margin: theme.spacing(3, 3, 3),
        borderRadius: 10,
        padding: theme.spacing(0, 0),
        maxWidth: 400,
    },
    section1: {
        margin: theme.spacing(3, 2),
    },
    section2: {
        margin: theme.spacing(2),
    },
    header: {
        alignItems: 'center',
        height: 'auto',
        paddingTop: theme.spacing(2),
        paddingLeft: theme.spacing(4),
        paddingBottom: theme.spacing(2),
        paddingRight: theme.spacing(2),
        backgroundColor: theme.palette.background.default,
    },
    img: {
        height: 255,
        maxWidth: 400,
        overflow: 'hidden',
        display: 'block',
        width: '100%',
    },
}));

export default function ObjavaElement(props) {
    const classes = useStyles();

    const tutorialSteps = [
        {
            path: props.objavaProp.slika ? props.objavaProp.slika : ''
        },
        {
            path: props.objavaProp.video ? props.objavaProp.video : ''
        }
    ]

    const theme = useTheme();
    const [activeStep, setActiveStep] = React.useState(0);
    const [komentar, setKomentar] = React.useState([]);
    const maxSteps = tutorialSteps.length;
    const noPointer = { cursor: 'default' };

    const handleNext = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    const MediaControl = () => {
        const slika = tutorialSteps[0].path;
        const video = tutorialSteps[1].path

        if (slika !== '' && video !== '') {
            return (
                <React.Fragment>
                    {activeStep === 0 ?
                        <img
                            className={classes.img}
                            src={tutorialSteps[activeStep].path}
                            alt={props.objavaProp.naslov}
                        />
                        :
                        <video className={classes.img} controls>
                            <source src={tutorialSteps[activeStep].path} type="video/mp4" />
                        </video>
                    }
                    <MobileStepper
                        steps={maxSteps}
                        position="static"
                        variant="text"
                        activeStep={activeStep}
                        nextButton={
                            <Button size="small" onClick={handleNext} disabled={activeStep === maxSteps - 1}>
                                Video
                                {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
                            </Button>
                        }
                        backButton={
                            <Button size="small" onClick={handleBack} disabled={activeStep === 0}>
                                {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
                                Slika
                            </Button>
                        }
                    />
                </React.Fragment>)
        } else if (slika !== '') {
            return (<img
                className={classes.img}
                src={tutorialSteps[0].path}
                alt={props.objavaProp.naslov}
            />)
        } else if (video !== '') {
            return (<video className={classes.img} controls>
                <source src={tutorialSteps[1].path} type="video/mp4" />
            </video>)
        } else {
            return null;
        }
    };

    const handleOdobri = (e) => {
        e.preventDefault();
        const id = props.objavaProp.id
        Axios.get('/api/odobriObjavu', {
            params: {
                id
            }
        })
            .then((response) => {
                if (response.data.objava != null) {
                    props.updateObjavaList(response.data.objava)
                }
            })
            .catch(function (error) {
                console.log(error);
                alert(error.response.status !== 500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
            });
    }

    const handleObrisi = () => {
        const id = props.objavaProp.id;
        Axios.delete('/api/obrisiObjavu', {
            params: {
                id
            }
        }).then((response) => {
            if (response.data.objava != null) {
                props.updateObjavaList(response.data.objava)
            }
        })
            .catch(function (error) {
                console.log(error);
                alert(error.response.status !== 500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
            });
    }

    const getKomentari = () => {
        const objavaID = props.objavaProp.id
        Axios.get('/api/komentariByObjava', {
            params: {
                objavaID
            }
        })
            .then((response) => {
                if (response.data != null) {
                    setKomentar(response.data)
                }
            })
            .catch(function (error) {
                console.log(error);
                alert(error.response.status !== 500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
            });
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        let komentar = {
            korisnikID: AuthenticationService.getLoggedInUserId(),
            objavaID: props.objavaProp.id,
            tekst: e.currentTarget.tekst.value
        }
        e.currentTarget.tekst.value = ""
        Axios.post('/api/komentar', komentar)
            .then((response) => {
                if (response.data != null) {
                    getKomentari(props.objavaProp.id)

                }
            })
            .catch(function (error) {
                console.log(error);
                alert(error.response.status !== 500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
            });
    }

    return (
        <div className={classes.root}>
            {!props.objavaProp.odobreno ?
                <Alert
                    severity="warning"
                    action={<React.Fragment>
                        <Grid item xs>
                            <Button color="inherit" onClick={handleOdobri}>
                                Odobri
                        </Button>
                            <Button color="inherit" onClick={() => {
                                if (window.confirm('Želite li obrisati ovu objavu?'))
                                    handleObrisi();
                            }}>
                                Obriši
                        </Button>
                        </Grid>
                    </React.Fragment>}
                >Ova objava još nije odobrena</Alert>
                : null}
            <Paper square elevation={0} className={classes.header} style={{ overflowWrap: "break-word" }}>
                <Grid container>
                    <Grid item xs={10}>
                        <Typography variant="h4">{props.objavaProp.naslov}</Typography>
                    </Grid>
                    {AuthenticationService.getLoggedInUserRole() === 'ROLE_ADMIN' ?
                        <Grid item xs={2}>
                            <Tooltip title="Obriši objavu">
                                <Button aria-label="delete" style={noPointer}>
                                    <DeleteIcon onClick={() => {
                                        if (window.confirm('Želite li obrisati ovu objavu?'))
                                            handleObrisi();
                                    }} />
                                </Button>
                            </Tooltip>
                        </Grid> : null
                    }
                </Grid>
                <Typography variant="h6" color="textSecondary" paragraph>Autor: {props.objavaProp.korisnik.username}</Typography>
                <Typography variant="h6" color="textSecondary" paragraph>{props.objavaProp.tekst}</Typography>
            </Paper>
            <MediaControl />
            <div>
                <Accordion onChange={(e, expanded) => {
                    if (expanded) {
                        getKomentari()
                    }
                }}>
                    <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1a-content" id="panel1a-header">
                        <Typography className={classes.heading}>Komentari</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Grid container spacing={3}>
                            <Grid item xs={12}>
                                <form className={classes.form} onSubmit={handleSubmit} style={{ display: "flex" }}>
                                    <TextField
                                        id="tekst"
                                        name="tekst"
                                        required
                                        label="Unesite svoj komentar"
                                        variant="outlined"
                                        fullWidth
                                        multiline
                                    />
                                    <Button type="submit" aria-label="delete">
                                        <CommentIcon />
                                    </Button>
                                </form>
                            </Grid>
                            {komentar.komentar != null ?
                                komentar.komentar.map((komentarProp, index) =>
                                    <Grid item xs={12} key={index} style={{ width: "-webkit-fill-available" }}>
                                        <Card>
                                            <CardContent style={{ overflowWrap: "break-word" }}>
                                                <Typography variant="body1" component="p">
                                                    {komentarProp.tekst}
                                                </Typography>
                                                <Typography className={classes.pos} color="textSecondary" align="right">
                                                    - {komentarProp.korisnik.username}
                                                </Typography>
                                            </CardContent>
                                        </Card>
                                    </Grid>) : null}
                        </Grid>
                    </AccordionDetails>
                </Accordion>
            </div>
        </div>
    );
}