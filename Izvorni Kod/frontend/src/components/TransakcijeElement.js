import React from "react";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";

export default function TransakcijeElement(props) {

  return (
    <TableRow key={props.id}>
      <TableCell component="th">{props.transakcijaProp.id}</TableCell>
      <TableCell component="th">{props.transakcijaProp.maketa.naziv}</TableCell>
      <TableCell component="th">
        {props.transakcijaProp.korisnik.username}
      </TableCell>
      <TableCell component="th">
        {props.transakcijaProp.materijal?.naziv}
      </TableCell>
      <TableCell component="th">{props.transakcijaProp.cijena} kn</TableCell>
      <TableCell allign="right">
        {props.transakcijaProp.datumkreiranja}
      </TableCell>
    </TableRow>
  );
}
