import React, { Component } from 'react'
import { Grid } from '@material-ui/core'
import Axios from 'axios';
import MaketaElement from './MaketaElement';
import AuthenticationService from './AuthenticationService';
import MaketaForm from './MaketaForm';
import VlastitaMaketaForm from './VlastitaMaketaForm';
import * as Const from './Constants';

export default class LandingPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            maketaList: null
        }
        this.getMaketaList = this.getMaketaList.bind(this);
        this.updateMaketaList = this.updateMaketaList.bind(this);
    }

    updateMaketaList(props) {
        if (props !== null) {
            this.setState({
                maketaList: props
            })
            this.getMaketaList();
        }
    }

    getMaketaList() {
        Axios.get('/api/maketaList')
            .then((response) => {
                if (response.data !== null) {
                    this.setState({
                        maketaList: response.data
                    })
                }
            })
            .catch(function (error) {
                console.log(error);
                alert(error.response.status !== 500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
            });
    }

    componentDidMount() {
        this.getMaketaList();
    }

    render() {
        return (
            <Grid style={{ padding: '35px' }} container >
                {(AuthenticationService.isUserLoggedIn() && AuthenticationService.getLoggedInUserRole() === 'ROLE_ADMIN') ?
                    <MaketaForm updateMaketaListProp={this.updateMaketaList} />
                    : null
                }
                {(AuthenticationService.isUserLoggedIn() && AuthenticationService.getLoggedInUserRole() === 'ROLE_USER') ?
                    <VlastitaMaketaForm updateMaketaListProp={this.updateMaketaList} />
                    : null
                }

                {this.state.maketaList !== null ?
                    this.state.maketaList.map((maketaProp, index) =>
                        <MaketaElement key={index} maketaProp={maketaProp} />) : null}

            </Grid>
        );
    }
}
