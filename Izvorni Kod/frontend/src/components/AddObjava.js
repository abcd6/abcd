import React from 'react'
import { Button, makeStyles } from '@material-ui/core'
import Axios from 'axios';

const useStyles = makeStyles((theme) => ({
    root: {
        margin: theme.spacing(12, 12),
    },
}));

export default function AddObjava(props) {

    const addNewObjava = () => {
        Axios.post('/api/addObjava', {
            tekst: "Objava 1"
        })
            .then(function (response) {
                props.updateObjavaListProp(response.data.objava);
                
            })
            .catch(function (error) {
                console.log(error);
                alert(error.response.status!==500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
            });
    }
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Button variant="contained" onClick={addNewObjava}>Dodaj objavu</Button>
        </div>
    )
}
