import React from "react";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Button from "@material-ui/core/Button";
import Axios from "axios";
import { TextField } from "@material-ui/core";
import * as Const from './Constants';
import AuthenticationService from "./AuthenticationService";

export default function NarudzbaElement(props) {
  var stanje = props.narudzbaProp.stanje;
  var tekstStanja;

  switch (stanje) {
    case 0:
      tekstStanja = "Nepotvrđena";
      break;
    case 1:
      tekstStanja = "Odobrena";
      break;
    case 2:
      tekstStanja = "Prihvaćena";
      break;
    default:
      break;
  }

  const handleOdobri = (e) => {
    if (document.getElementById(props.narudzbaProp.id).value !== "") {
      const request = { narudzbaId: props.narudzbaProp.id, cijena: document.getElementById(props.narudzbaProp.id).value }
      const id = props.narudzbaProp.id;
      if (props.narudzbaProp.stanje === 0) {
        Axios.post("/api/odobriNarudzbuAdmin",
          request
        )
          .then((response) => {
            if (response.data.narudzba != null) {
              props.updateNarudzbaList(response.data.narudzba);
            }
          })
          .catch(function (error) {
            console.log(error);
            alert(error.response.status !== 500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
          });
      }
      else if (props.narudzbaProp.stanje === 1) {
        Axios.get("/api/odobriNarudzbuKorisnik", {
          params: {
            id
          }
        })
          .then((response) => {
            if (response.data.narudzba != null) {
              props.updateNarudzbaList(response.data.narudzba);
            }
          })
          .catch(function (error) {
            console.log(error);
            alert(error.response.status !== 500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
          });
      }
    } else {
      alert("Potrebno je unijeti predloženu cijenu")
    }
  };

  const handleObrisi = () => {
    const id = props.narudzbaProp.id;
    Axios.delete('/api/obrisiNarudzbu', {
      params: {
        id
      }
    })
      .then((response) => {
        if (response.data.narudzba != null) {
          props.updateNarudzbaList(response.data.narudzba);
        }
      })
      .catch(function (error) {
        console.log(error);
        alert(error.response.status !== 500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
      });
  };

  const [cijena_1, setCijena_1] = React.useState(false);

  const handleOnClick = () => {
    console.log(props)
    let volumen = props.narudzbaProp.maketa.širina * props.narudzbaProp.maketa.visina * props.narudzbaProp.maketa.dužina;
    let value = (volumen * 5 + volumen * props.narudzbaProp.materijal.cijena).toFixed(2);
    setCijena_1(value + " kn");
  }

  return (
    <TableRow key={props.id}>
      <TableCell component="th">{props.narudzbaProp.id}</TableCell>
      <TableCell component="th">{props.narudzbaProp.maketa.naziv}</TableCell>
      <TableCell component="th">{props.narudzbaProp.materijal.naziv}</TableCell>
      <TableCell allign="right">{props.narudzbaProp.datumkreiranja}</TableCell>
      <TableCell allign="right">{tekstStanja}</TableCell>
      <TableCell>
        {props.narudzbaProp.stanje === 0 && AuthenticationService.getLoggedInUserRole() === 'ROLE_ADMIN' ?
          <React.Fragment><Button onClick={handleOnClick}>Predloži cijenu</Button>{cijena_1}</React.Fragment> : null}
      </TableCell>
      <TableCell allign="right">
        <TextField id={props.narudzbaProp.id + ""} name="cijena" value={props.narudzbaProp?.cijena} disabled={props.narudzbaProp.stanje !== 0 || AuthenticationService.getLoggedInUserRole() === 'ROLE_USER'} />
      </TableCell>
      <TableCell>
        {props.narudzbaProp.stanje !== 2 ?
          <React.Fragment>
            {props.narudzbaProp.stanje === 0 && AuthenticationService.getLoggedInUserRole() === 'ROLE_ADMIN' ?
              <Button color="inherit" size="small" onClick={handleOdobri}>
                Odobri
          </Button> : null}
            {props.narudzbaProp.stanje === 1 && AuthenticationService.getLoggedInUserRole() === 'ROLE_USER' ?
              <Button color="inherit" size="small" onClick={() => {
                if (window.confirm('Prihvaćanjem ponude potvrđujete narudžbu, informacije o plaćanju će te dobiti na email adresu!'))
                  handleOdobri();
              }}>
                Prihvati
          </Button> : null}
          </React.Fragment>
          : null}
      </TableCell>
      <TableCell>
        {props.narudzbaProp.stanje !== 2 ?
          <Button color="inherit" size="small" onClick={handleObrisi}>
            Obriši
        </Button> : null}
      </TableCell>
    </TableRow>
  );
}
