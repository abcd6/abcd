import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import { Button, TextField } from "@material-ui/core";
import Box from '@material-ui/core/Box';
import Axios from 'axios';
import AuthenticationService from './AuthenticationService';
import * as Const from './Constants';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        color: '#595959',
        margin: theme.spacing(6, 0),
    },
    sections: {
        backgroundColor: '#f2f2f2'
    },
    section: {
        margin: theme.spacing(3, 2, 0, 2),
        padding: theme.spacing(2, 2, 1, 2)
    },
    section1: {
        margin: theme.spacing(3, 2, 0, 2),
        padding: theme.spacing(2, 2, 1, 2)
    },
    section2: {
        margin: theme.spacing(2, 2, 0, 2),
        padding: theme.spacing(0, 2, 1, 2)
    },
    button: {
        margin: theme.spacing(2, 2, 2, 2)
    },
    label: {
        color: '#333333'
    }
}));

let osobniPodaci = {
    id: "",
    ime: "",
    prezime: "",
    datumrođenja: "",
    mobitel: "",
    email: "",
    adresa: "",
    država: "",
    poštanskibroj: "",
    datumkreiranjaracuna: ""
}


export default function OsobniPodaciDetail(props) {
    const classes = useStyles();
    const [v, setV] = React.useState(false);

    const handleClickOpen = (e) => {
        setV(true);
    }

    const handleClickClose = (e) => {
        setV(false);
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        osobniPodaci.id = AuthenticationService.getLoggedInUserId();
        osobniPodaci.ime = e.target.ime.value;
        osobniPodaci.prezime = e.target.prezime.value;
        osobniPodaci.datumrođenja = e.target.datumrođenja.value;
        osobniPodaci.mobitel = e.target.mobitel.value;
        osobniPodaci.email = e.target.email.value;
        osobniPodaci.adresa = e.target.adresa.value;
        osobniPodaci.država = e.target.država.value;
        osobniPodaci.poštanskibroj = e.target.poštanskibroj.value;

        Axios.post('/api/osobniPodaci',
            osobniPodaci
        )
            .then(function (response) {
                props.updateOsobniPodaciProp(response.data);
                
                setV(false)
            })
            .catch(function (error) {
                console.log(error);
                alert(error.response.status !== 500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
            });
    }

    let id = AuthenticationService.getLoggedInUserId();

    const handleDelete = (e) => {

        // treba dodat neki alert prije brisanja, nez kako to
        // let c = confirm("Jeste li sigurni da želite isbrisati korisnički račun? ");
        if (true) {
            Axios.get('/api/osobniPodaci', { params: { id } }
            ).then(function (response) {
                AuthenticationService.logout();
            }).catch(function (error) {
                console.log(error);
                alert(error.response.status !== 500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
            })
        }
    }

    return (
        <div>
            {v ?
                <form className={classes.root} onSubmit={handleSubmit}>
                    <Typography variant="h4">Uređivanje korisničkog računa</Typography>
                    <Box className={classes.sections} boxShadow={3}>
                        <Box className={classes.section}>
                            <TextField id="ime" label="Ime" defaultValue={props.osobniPodaciProp.ime} name="ime" />
                        </Box>
                        <Box className={classes.section}>
                            <TextField id="prezime" label="Prezime" defaultValue={props.osobniPodaciProp.prezime} name="prezime" />
                        </Box>
                        <Box className={classes.section}>
                            <TextField id="datumrođenja" label="Datum rođenja" defaultValue={props.osobniPodaciProp.datumrođenja} name="datumrođenja" />
                        </Box>
                        <Box className={classes.section}>
                            <TextField id="mobitel" label="Mobitel" defaultValue={props.osobniPodaciProp.mobitel} name="mobitel" />
                        </Box>
                        <Box className={classes.section}>
                            <TextField id="email" label="Email" defaultValue={props.osobniPodaciProp.email} name="email" />
                        </Box>
                        <Box className={classes.section}>
                            <TextField id="adresa" label="Adresa" defaultValue={props.osobniPodaciProp.adresa} name="adresa" />
                        </Box>
                        <Box className={classes.section}>
                            <TextField id="država" label="Država" defaultValue={props.osobniPodaciProp.država} name="država" />
                        </Box>
                        <Box className={classes.section}>
                            <TextField id="poštanskibroj" label="Poštanski broj" defaultValue={props.osobniPodaciProp.poštanskibroj} name="poštanskibroj" />
                        </Box>
                        <Box className={classes.section}>
                            <Button className={classes.button} variant="contained" type="submit">Spremi</Button>
                            <Button className={classes.button} variant="contained" onClick={handleClickClose}>Odbaci</Button>
                        </Box>
                    </Box>
                    <Button className={classes.button} variant="contained" onClick={() => {
                                        if (window.confirm('Želite li obrisati korisnički račun?'))
                                        handleDelete();
                                    }}>Izbriši korisnički račun</Button>
                </form>
                :
                <div className={classes.root}>
                    <Typography variant="h4">Korisnički račun</Typography>
                    <Grid container>
                        <Box className={classes.sections} boxShadow={3}>
                            <Box className={classes.section1}>
                                <Grid item xs>
                                    <Typography gutterBottom variant="h4">
                                        {props.osobniPodaciProp.ime} {props.osobniPodaciProp.prezime}
                                    </Typography>
                                </Grid>
                            </Box>
                            <Divider />
                            <Box className={classes.section2}>
                                <Grid item>
                                    <Box>
                                        <Typography gutterBottom variant="h6">
                                            Datum rođenja: {props.osobniPodaciProp.datumrođenja}
                                        </Typography>
                                    </Box>
                                </Grid>
                                <Grid item>
                                    <Typography gutterBottom variant="h6">
                                        Mobitel: {props.osobniPodaciProp.mobitel}
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Typography gutterBottom variant="h6">
                                        Email: {props.osobniPodaciProp.email}
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Typography gutterBottom variant="h6">
                                        Adresa: {props.osobniPodaciProp.adresa}
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Typography gutterBottom variant="h6">
                                        Država: {props.osobniPodaciProp.država}
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Typography gutterBottom variant="h6">
                                        Poštanski broj: {props.osobniPodaciProp.poštanskibroj}
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Typography gutterBottom variant="h6">
                                        Datum kreiranja računa: {props.osobniPodaciProp.datumkreiranja}
                                    </Typography>
                                </Grid>
                                <Grid className={classes.button} item>
                                    <Button color="inherit" variant="contained" onClick={handleClickOpen}>
                                        Uredi
                                    </Button>
                                </Grid>
                            </Box>
                        </Box>
                    </Grid>
                </div>
            }
        </div>
    )
}
