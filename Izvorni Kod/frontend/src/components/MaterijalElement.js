import React from 'react'
import { Checkbox, FormControlLabel } from '@material-ui/core'

export default function MaterijalElement(props) {

    const handleChange = (e) => {
        if (e.target.checked) {
            props.updateMaterijalList(props.materijalProp)
        } else {
            props.deleteElementMaterijalList(props.materijalProp)
        }
    }

    return (
        <React.Fragment>
            <FormControlLabel
                control={<Checkbox id={props.materijalProp.id+""} name={props.materijalProp.naziv} color="primary" />}
                label={props.materijalProp.naziv} onChange={handleChange}
            />
        </React.Fragment>
    );
}