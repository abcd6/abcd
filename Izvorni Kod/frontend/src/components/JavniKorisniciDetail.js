import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 500,
        color: '#595959',
        margin: theme.spacing(6, 4),
    },
    sections: {
        backgroundColor: '#f2f2f2'
    },
    section: {
        margin: theme.spacing(3, 2, 0, 2),
        padding: theme.spacing(2, 2, 1, 2)
    },
    section1: {
        margin: theme.spacing(3, 2, 0, 2),
        padding: theme.spacing(2, 2, 1, 2)
    },
    section2: {
        margin: theme.spacing(2, 2, 0, 2),
        padding: theme.spacing(0, 2, 1, 2)
    },
    button: {
        margin: theme.spacing(2, 2, 2, 2)
    },
    label: {
        color: '#333333'
    }
}));

export default function JavniKorisniciDetail(props) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Box className={classes.sections} boxShadow={3}>
                <Box className={classes.section1}>
                    <Grid item xs>
                        <Typography gutterBottom variant="h4">
                            {props.osobniPodaciProp.ime} {props.osobniPodaciProp.prezime}
                        </Typography>
                    </Grid>
                </Box>
                <Divider />
                <Box className={classes.section2}>
                    <Grid item>
                        <Box>
                            <Typography gutterBottom variant="h6">
                                Datum rođenja: {props.osobniPodaciProp.datumrođenja}
                            </Typography>
                        </Box>
                    </Grid>
                    <Grid item>
                        <Typography gutterBottom variant="h6">
                            Mobitel: {props.osobniPodaciProp.mobitel}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography gutterBottom variant="h6">
                            Email: {props.osobniPodaciProp.email}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography gutterBottom variant="h6">
                            Adresa: {props.osobniPodaciProp.adresa}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography gutterBottom variant="h6">
                            Država: {props.osobniPodaciProp.država}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography gutterBottom variant="h6">
                            Poštanski broj: {props.osobniPodaciProp.poštanskibroj}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography gutterBottom variant="h6">
                            Datum kreiranja računa: {props.osobniPodaciProp.datumkreiranja}
                        </Typography>
                    </Grid>
                </Box>
            </Box>
        </div>
    )
}
