import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
  listItem: {
    padding: theme.spacing(1, 0),
  },
  total: {
    fontWeight: 700,
  },
  title: {
    marginTop: theme.spacing(2),
  },
}));

export default function Review(props) {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Sažetak narudžbe
      </Typography>
      <Typography >{props.maketa.naziv}</Typography>
      <Typography >{props.maketa.cijena} kn</Typography>
      <Typography >{props.materijal.naziv}</Typography>

      <Grid container spacing={2}>
        <Grid item xs={12} sm={6}>
          <Typography variant="h6" gutterBottom className={classes.title}>
            Podaci dostave
          </Typography>
          <Typography gutterBottom>{props.korisnik.ime} {props.korisnik.prezime}</Typography>
          <Typography gutterBottom>{props.korisnik.adresa}</Typography>
          <Typography gutterBottom>{props.korisnik.postanskiBroj}</Typography>
          <Typography gutterBottom>{props.korisnik.drzava}</Typography>
        </Grid>
        <Grid item container direction="column" xs={12} sm={6}>
          <Typography variant="h6" gutterBottom className={classes.title}>
            Detalji plaćanja
          </Typography>

          <Typography gutterBottom>{props.kartica.imeKartice}</Typography>
          <Typography gutterBottom>{props.kartica.brojKartice}</Typography>
          <Typography gutterBottom>{props.kartica.datumIstek}</Typography>
          <Typography gutterBottom>{props.kartica.cvv}</Typography>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}