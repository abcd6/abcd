import React from 'react'
import { Button, Box, Grid, makeStyles, TextField, Typography, Dialog } from '@material-ui/core';
import Axios from 'axios';
import MaterialPicker from './MaterialPicker';
import * as Const from './Constants';
import InputAdornment from '@material-ui/core/InputAdornment';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        margin: theme.spacing(0, 3, 0),
        borderRadius: 20,
        padding: theme.spacing(2, 2)
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    form: {
        width: '100%', // IE 11
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
        backgroundColor: '#dfd3d3',
    },
    materialForm: {
        width: '100%', // IE 11
        margin: theme.spacing(3, 0, 0, 1)
    }
}));

let materijalList = [];
let maketa = {
    naziv: "",
    opis: "",
    visina: "",
    širina: "",
    dužina: "",
    mjerilo: "",
    boja: "",
    cijena: "",
    standardna: "",
    slika: ""
}

export default function MaketaForm(props) {

    const classes = useStyles();
    const handleSubmit = (e) => {
        e.preventDefault()

        maketa.naziv = e.target.naziv.value;
        maketa.opis = e.target.opis.value;
        maketa.visina = e.target.visina.value;
        maketa.širina = e.target.širina.value;
        maketa.dužina = e.target.dužina.value;
        maketa.mjerilo = e.target.mjerilo.value;
        maketa.boja = e.target.boja.value;
        maketa.cijena = e.target.cijena.value;
        maketa.standardna = true;

        Axios.post('/api/addMaketa',
            { maketa, materijalList }
        )
            .then(function (response) {
                props.updateMaketaListProp(response.data);
                
                maketa = {
                    naziv: "",
                    opis: "",
                    visina: "",
                    širina: "",
                    dužina: "",
                    mjerilo: "",
                    boja: "",
                    cijena: "",
                    standardna: "",
                    slika: ""
                }
                materijalList = [];
                setOpen(false)
            })
            .catch(function (error) {
                console.log(error);
                alert(error.response.status !== 500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
            });

    }

    const updateMaterijalList = (props) => {
        materijalList.push(props);
    }

    const deleteElementMaterijalList = (props) => {

        const index = materijalList.indexOf(props);
        if (index > -1) {
            materijalList.splice(index, 1);
        }
    }

    const [cijena, setCijena] = React.useState(false);

    const updateCijena = (e) => {
        let volumen = document.getElementById("širina").value * document.getElementById("visina").value * document.getElementById("dužina").value;
        // volumen * fiksna cijena rada
        let value = volumen * 5;
        console.log(cijena);
        setCijena(value);
    };

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };


    const getBase64 = (file, cb) => {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            cb(reader.result)
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }


    const onFileChange = (e) => {
        getBase64(e.target.files[0], (result) => {
            maketa.slika = result;
        });
    }

    return (
        <div className={classes.root}>
            <Button variant="outlined" color="primary" onClick={handleClickOpen}>
                Stvori maketu
            </Button>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <div className={classes.paper}>
                    <Typography component="h1" variant="h5">
                        Stvori maketu
                    </Typography>
                    <form className={classes.form} onSubmit={handleSubmit}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    autoFocus
                                    required
                                    fullWidth
                                    id="naziv"
                                    name="naziv"
                                    label={Const.MAKETANAME}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    id="opis"
                                    name="opis"
                                    label={Const.MAKETADESCRIPTION}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    id="visina"
                                    name="visina"
                                    label={Const.HEIGHT}
                                    InputProps={{
                                        endAdornment: <InputAdornment position="end">cm</InputAdornment>,
                                    }}
                                />
                            </Grid>

                            <Grid item xs={12} sm={6}>
                                <TextField
                                    required
                                    fullWidth
                                    id="širina"
                                    name="širina"
                                    label={Const.WIDTH}
                                    InputProps={{
                                        endAdornment: <InputAdornment position="end">cm</InputAdornment>,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    required
                                    fullWidth
                                    id="dužina"
                                    name="dužina"
                                    label={Const.LENGTH}
                                    InputProps={{
                                        endAdornment: <InputAdornment position="end">cm</InputAdornment>,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    id="mjerilo"
                                    name="mjerilo"
                                    label={Const.MEASUREMENT}
                                />
                            </Grid>
                            <Grid className={classes.materialForm}>
                                <MaterialPicker updateMaterijalList={updateMaterijalList} deleteElementMaterijalList={deleteElementMaterijalList} updateCijena={updateCijena} />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    id="boja"
                                    name="boja"
                                    label={Const.MAKETACOLOR}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Button onClick={updateCijena}>Prikaži predloženu cijenu</Button>
                            </Grid>
                            <Grid item xs={12}>
                                <Typography component="h1" variant="h6" onClick={updateCijena}>
                                    Cijena: {cijena ? cijena + " kn" : "0 kn"}
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    id="cijena"
                                    name="cijena"
                                    label={Const.PRICE}
                                    InputProps={{
                                        endAdornment: <InputAdornment position="end">kn</InputAdornment>,
                                    }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <label>Dodaj sliku</label>
                            </Grid>
                            <Grid item xs={12}>
                                <input id="slika" type="file" accept="image/png, image/jpeg" onChange={onFileChange} />
                            </Grid>
                        </Grid>

                        <Button
                            type="submit"
                            variant="contained"
                            className={classes.submit}
                            onClick={handleClose}
                        >
                            <Box fontWeight="fontWeightBold" m={1}>
                                {Const.CREATEMAKETA}
                            </Box>
                        </Button>
                    </form>
                </div>
            </Dialog>
        </div>
    );
}
