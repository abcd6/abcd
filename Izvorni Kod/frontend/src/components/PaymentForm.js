import React from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';

export default function PaymentForm(props) {
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Metoda plaćanja
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12} md={6}>
          <TextField required id="cardName" label="Ime na kartici" fullWidth autoComplete="cc-name" defaultValue={props.karticaProp.imeKartice} />
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            required
            id="cardNumber"
            label="Broj kartice"
            fullWidth
            autoComplete="cc-number"
            defaultValue={props.karticaProp.brojKartice}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField required id="expDate" label="Datum isteka kartice" fullWidth autoComplete="cc-exp" defaultValue={props.karticaProp.datumIstek} />
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            required
            id="cvv"
            label="CVV"
            helperText="3 znamenke u polju potpisa"
            fullWidth
            autoComplete="cc-csc"
            defaultValue={props.karticaProp.cvv}
          />
        </Grid>
      </Grid>
    </React.Fragment>
  );
}