import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';

export default function AddressForm(props) {
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Adresa pošiljke
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="firstName"
            name="firstName"
            label="Ime"
            fullWidth
            autoComplete="given-name"
            defaultValue={props.korisnikProp.ime}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="lastName"
            name="lastName"
            label="Prezime"
            fullWidth
            autoComplete="family-name"
            defaultValue={props.korisnikProp.prezime}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            required
            id="address1"
            name="address1"
            label="Adresa"
            fullWidth
            autoComplete="shipping address-line1"
            defaultValue={props.korisnikProp.adresa}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="zip"
            name="zip"
            label="Poštanski broj"
            fullWidth
            autoComplete="shipping postal-code"
            defaultValue={props.korisnikProp.postanskiBroj}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="country"
            name="country"
            label="Država"
            fullWidth
            autoComplete="shipping country"
            defaultValue={props.korisnikProp.drzava}
          />
        </Grid>
      </Grid>
    </React.Fragment>
  );
}