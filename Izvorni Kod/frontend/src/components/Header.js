import React from 'react'
import {
    AppBar,
    Button,
    makeStyles,
    Menu,
    MenuItem,
    Toolbar,
    Typography,
} from "@material-ui/core";
import * as Const from "./Constants";
import { NavLink } from "react-router-dom";
import { AccountCircle } from "@material-ui/icons";
import AuthenticationService from './AuthenticationService';
import { useHistory } from "react-router";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    objava: {
        position: 'relative',
        //   borderRadius: theme.shape.borderRadius,
        //   marginRight: theme.spacing(2),
        //   marginLeft: 0,
    }
}));

export default function Header(props) {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
   /*  const [loggedOut, setloggedOut] = React.useState(!props.userLoggedIn); */
    const [open, setOpen] = React.useState(false);
    const history = useHistory();

    /* useEffect(() => {
        if (!props.userLoggedIn !== loggedOut) {
            setloggedOut(!props.userLoggedIn)
        }
    }, [props.userLoggedIn, loggedOut]) */

    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
        setOpen(true)
    };

    const handleLogOut = () => {
        AuthenticationService.logout();
        props.setuserLoggedIn(false);
        setOpen(false)
        history.push("/")
    };

    const handleClose = () => {
        setAnchorEl(null);
        setOpen(false);
    };

    return (
        <div className={classes.root}>
            <AppBar position="fixed">
                <Toolbar>
                    <Typography variant="h6" className={classes.title} >
                        <Button color="inherit" component={NavLink} to="/">
                            {Const.MAKETASHOP}
                        </Button>
                    </Typography>
                    <Typography variant="h6" className={classes.objava}>
                        <Button color="inherit" component={NavLink} to="/objava">
                            {Const.OBJAVE}
                        </Button>
                    </Typography>
                    {!props.userLoggedIn ?
                        <Button color="inherit" component={NavLink} to="/login">
                            {Const.LOGIN}
                        </Button>
                        :
                        <div>
                            <Button color="inherit" component={NavLink} to="/osobniPodaciKorisnik">
                                {AuthenticationService.getLoggedInUserName()}
                            </Button>
                            <Button
                                aria-label="account of current user"
                                aria-controls="menu-appbar"
                                aria-haspopup="true"
                                onClick={handleMenu}
                                color="inherit"
                            >
                                <AccountCircle />
                            </Button>
                            <Menu
                                id="menu-appbar"
                                anchorEl={anchorEl}
                                anchorOrigin={{
                                    vertical: "top",
                                    horizontal: "right",
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: "top",
                                    horizontal: "right",
                                }}
                                open={open}
                                onClose={handleClose}
                            >
                                <MenuItem component={NavLink} to="/narudzbe">{Const.NARUDZBE}</MenuItem>
                                <MenuItem component={NavLink} to="/transakcije">{Const.TRANSAKCIJE}</MenuItem>
                                {AuthenticationService.getLoggedInUserName() === 'admin' ? <MenuItem component={NavLink} to="/osobniPodaciAdmin">Korisnici</MenuItem> : null}
                                {AuthenticationService.getLoggedInUserName() !== 'admin' ? <MenuItem component={NavLink} to="/javniOsobniPodaci">Korisnici</MenuItem> : null}
                                <MenuItem onClick={handleLogOut}>Odjavite se</MenuItem>
                            </Menu>
                        </div>
                    }
                </Toolbar>
            </AppBar>
            <br />
        </div>
    );
}
