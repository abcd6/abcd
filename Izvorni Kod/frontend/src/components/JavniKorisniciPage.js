import React, { Component } from 'react'
import { Grid } from '@material-ui/core'
import Axios from 'axios';
import JavniKorisniciDetail from './JavniKorisniciDetail';
import * as Const from './Constants';

export default class JavniKorisniciPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            osobniPodaci: null
        }
        this.getOsobniPodaci = this.getOsobniPodaci.bind(this);
    }

    getOsobniPodaci() {
        Axios.get('/api/javniOsobniPodaci')
            .then((response) => {
                if (response.data.osobniPodaci != null) {
                    this.setState({
                        osobniPodaci: response.data.osobniPodaci
                    })
                }
            })
            .catch(function (error) {
                console.log(error);
                alert(error.response.status!==500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
            });
    }

    componentDidMount() {
        this.getOsobniPodaci();
    }


    render() {
        return (
            <Grid container>
                {this.state.osobniPodaci != null ?
                    this.state.osobniPodaci.map((osobniPodaciProp, index) =>
                        <JavniKorisniciDetail
                            key={index}
                            osobniPodaciProp={osobniPodaciProp}
                        />) : null}
            </Grid>
        );
    }
}
