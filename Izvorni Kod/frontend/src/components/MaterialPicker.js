import React, { Component } from 'react'
import { Grid } from '@material-ui/core'
import Axios from 'axios';
import MaterijalElement from './MaterijalElement';
import * as Const from './Constants';

export default class MaterialPicker extends Component {

    constructor(props) {
        super(props);
        this.state = {
            materijalList: null,
            checkedValues: []
        }
        this.getMaterijalList = this.getMaterijalList.bind(this);
    }

    getMaterijalList() {
        Axios.get('/api/materijali')
            .then((response) => {
                if (response.data.materijal != null) {
                    this.setState({
                        materijalList: response.data.materijal
                    })
                }
            })
            .catch(function (error) {
                console.log(error);
                alert(error.response.status!==500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
            });
    }

    componentDidMount() {
        this.getMaterijalList();
    }



    render() {
        return (
            <Grid>
                <Grid item xs={12}>
                    <label color="primary">Odaberi materijal:</label>
                </Grid>
                {this.state.materijalList != null ?
                    this.state.materijalList.map((materijalProp, index) =>

                        <MaterijalElement key={index} materijalProp={materijalProp} updateMaterijalList={this.props.updateMaterijalList}
                            deleteElementMaterijalList={this.props.deleteElementMaterijalList} />) : null}
            </Grid>


        );
    }
}