import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import LockIcon from '@material-ui/icons/Lock';
import UnlockIcon from '@material-ui/icons/LockOpen';
import Axios from 'axios';
import { Button } from '@material-ui/core';
import * as Const from './Constants';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 500,
        color: '#595959',
        margin: theme.spacing(6, 4),
    },
    sections: {
        backgroundColor: '#f2f2f2'
    },
    section: {
        margin: theme.spacing(3, 2, 0, 2),
        padding: theme.spacing(2, 2, 1, 2)
    },
    section1: {
        margin: theme.spacing(3, 2, 0, 2),
        padding: theme.spacing(2, 2, 1, 2)
    },
    section2: {
        margin: theme.spacing(2, 2, 0, 2),
        padding: theme.spacing(0, 2, 1, 2)
    },
    button: {
        margin: theme.spacing(2, 2, 2, 2)
    },
    label: {
        color: '#333333'
    }
}));

export default function OsobniPodaciDetail(props) {
    const classes = useStyles();

    const handleStatus = (e) => {
        e.preventDefault();
        const id = props.osobniPodaciProp.korisnik.id
        Axios.get('/api/statusKorisnik', {
            params: {
                id
            }
        })
            .then((response) => {
                if (response.data.osobniPodaci != null) {
                    props.updateOsobniPodaciProp(response.data.osobniPodaci);
                }
            })
            .catch(function (error) {
                console.log(error);
                alert(error.response.status!==500 ? error.response.data : Const.SERVER_CONNECTION_ERROR);
            });
    }

    return (
        <div className={classes.root}>
            <Box className={classes.sections} boxShadow={3}>
                <Box className={classes.section1}>
                    <Grid item xs>
                        <Typography gutterBottom variant="h4">
                            {props.osobniPodaciProp.ime} {props.osobniPodaciProp.prezime}
                        </Typography>
                    </Grid>
                </Box>
                <Divider />
                <Box className={classes.section2}>
                    <Grid item>
                        <Box>
                            <Typography gutterBottom variant="h6">
                                Datum rođenja: {props.osobniPodaciProp.datumrođenja}
                            </Typography>
                        </Box>
                    </Grid>
                    <Grid item>
                        <Typography gutterBottom variant="h6">
                            Mobitel: {props.osobniPodaciProp.mobitel}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography gutterBottom variant="h6">
                            Email: {props.osobniPodaciProp.email}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography gutterBottom variant="h6">
                            Adresa: {props.osobniPodaciProp.adresa}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography gutterBottom variant="h6">
                            Država: {props.osobniPodaciProp.država}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography gutterBottom variant="h6">
                            Poštanski broj: {props.osobniPodaciProp.poštanskibroj}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography gutterBottom variant="h6">
                            Datum kreiranja računa: {props.osobniPodaciProp.datumkreiranja}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography gutterBottom variant="h6">
                            Status: {props.osobniPodaciProp.korisnik?.aktivan ? 'Aktivan' : 'Neaktivan'}
                            <Button onClick={handleStatus}>
                                {props.osobniPodaciProp.korisnik?.aktivan ? <LockIcon /> : <UnlockIcon />}
                            </Button>
                        </Typography>
                    </Grid>
                </Box>
            </Box>
        </div>
    )
}
