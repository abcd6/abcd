import React from 'react';
import Login from './Login';
import Register from './Register';
import LandingPage from './LandingPage';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Header from './Header';
import { makeStyles } from '@material-ui/core';
import Footer from './Footer';
import ObjavePage from './ObjavePage';
import NarudzbaList from './NarudzbaList'
import TransakcijePage from './TransakcijePage';
import Checkout from './Checkout';
import MaketaForm from './MaketaForm';
import OsobniPodaciPage from './OsobniPodaciPage';
import AuthenticationService from './AuthenticationService';
import KorisniciAdminPage from './KorisniciAdminPage';
import JavniKorisniciPage from './JavniKorisniciPage';

const useStyles = makeStyles((theme) => ({
  root: {
    minHeight: '75vh',
    margin: theme.spacing(2, 4),
  },
}));

function App() {
  const [userLoggedIn, setuserLoggedIn] = React.useState(AuthenticationService.isUserLoggedIn());
  const classes = useStyles();
  if(AuthenticationService.isUserLoggedIn()){
    AuthenticationService.setupAxiosInterceptors(AuthenticationService.createJWTToken(AuthenticationService.getLoggedInUserToken()));
  }
  return (
    <BrowserRouter>
      <Header userLoggedIn={userLoggedIn} setuserLoggedIn={setuserLoggedIn} />
      <div className={classes.root}>
        <Switch>
          <Route path="/login">
            <Login setuserLoggedIn={setuserLoggedIn} />
          </Route>
          <Route path="/register">
            <Register />
          </Route>
          <Route path="/objava">
            <ObjavePage />
          </Route>
          <Route path="/narudzbe">
            <NarudzbaList />
          </Route>
          <Route path="/transakcije">
            <TransakcijePage />
          </Route>
          <Route path="/placanje">
            <Checkout />
          </Route>
          <Route path="/dodajMaketu">
            <MaketaForm />
          </Route>
          <Route path="/osobniPodaciKorisnik">
            <OsobniPodaciPage />
          </Route>
          <Route path="/javniOsobniPodaci">
            <JavniKorisniciPage />
          </Route>
          <Route path="/osobniPodaciAdmin">
            <KorisniciAdminPage />
          </Route>
          <Route path="/">
            <LandingPage />
          </Route>
        </Switch>
      </div>
      <Footer />


    </BrowserRouter>
  );
}

export default App;
