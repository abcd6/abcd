package hr.fer.ppi.abcd.maketashop.service;

import hr.fer.ppi.abcd.maketashop.security.JwtUserDetails;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import java.util.Arrays;

@TestConfiguration
public class SpringSecurityWebAuxTestConfig {

    @Bean
    @Primary
    public UserDetailsService userDetailsService() {
        return new InMemoryUserDetailsManager(Arrays.asList(
                new JwtUserDetails(1L, "admin", "admin", "ROLE_ADMIN"),
                new JwtUserDetails(2L, "anon", "", "ROLE_ANON")
        ));
    }
}
