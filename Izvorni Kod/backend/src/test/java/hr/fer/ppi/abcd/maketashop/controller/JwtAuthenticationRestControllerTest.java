package hr.fer.ppi.abcd.maketashop.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import hr.fer.ppi.abcd.maketashop.bean.UserDTO;
import hr.fer.ppi.abcd.maketashop.model.Korisnik;
import hr.fer.ppi.abcd.maketashop.model.OsobniPodaci;
import hr.fer.ppi.abcd.maketashop.service.SpringSecurityWebAuxTestConfig;
import hr.fer.ppi.abcd.maketashop.service.impl.JwtInMemoryUserDetailsServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = SpringSecurityWebAuxTestConfig.class
)
@AutoConfigureMockMvc
class JwtAuthenticationRestControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private JwtInMemoryUserDetailsServiceImpl jwt;

    @Test
    void saveUser_OK_donjaGranica() throws Exception {
        UserDTO userDto = new UserDTO();
        Korisnik korisnik = new Korisnik();
        korisnik.setUsername("marko_i");
        korisnik.setPassword("marko_i");
        userDto.setKorisnik(korisnik);

        OsobniPodaci osobniPodaci = new OsobniPodaci();
        osobniPodaci.setIme("Marko");
        osobniPodaci.setPrezime("Ilic");
        osobniPodaci.setMobitel("0985478552");
        osobniPodaci.setEmail("marko@gmail.com");
        osobniPodaci.setAdresa("adresa");
        osobniPodaci.setDržava("Hrvatska");
        osobniPodaci.setPoštanskibroj(10000);
        osobniPodaci.setPrivatni(false);

        userDto.setOsobniPodaci(osobniPodaci);
        userDto.setPassword("marko_i");

        mvc.perform(post("/api/register").accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(userDto)))
                .andExpect(status().isOk());
    }

    @Test
    void saveUser_FAIL_donjaGranica() throws Exception {
        UserDTO userDto = new UserDTO();
        Korisnik korisnik = new Korisnik();
        korisnik.setUsername("marko_j");
        korisnik.setPassword("marko_j");
        userDto.setKorisnik(korisnik);

        OsobniPodaci osobniPodaci = new OsobniPodaci();
        osobniPodaci.setIme("Marko");
        osobniPodaci.setPrezime("Ilic");
        osobniPodaci.setMobitel("0985478552");
        osobniPodaci.setEmail("marko@gmail.com");
        osobniPodaci.setAdresa("adresa");
        osobniPodaci.setDržava("Hrvatska");
        osobniPodaci.setPoštanskibroj(9999);
        osobniPodaci.setPrivatni(false);

        userDto.setOsobniPodaci(osobniPodaci);
        userDto.setPassword("marko_i");

        mvc.perform(post("/api/register").accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(userDto)))
                .andExpect(content().string("DoÅ¡lo je do pogreÅ¡ke prilikom registracije"));
    }

    @Test
    void saveUser_OK_gornjaGranica() throws Exception {
        UserDTO userDto = new UserDTO();
        Korisnik korisnik = new Korisnik();
        korisnik.setUsername("marko_k");
        korisnik.setPassword("marko_i");
        userDto.setKorisnik(korisnik);

        OsobniPodaci osobniPodaci = new OsobniPodaci();
        osobniPodaci.setIme("Marko");
        osobniPodaci.setPrezime("Ilic");
        osobniPodaci.setMobitel("0985478552");
        osobniPodaci.setEmail("marko@gmail.com");
        osobniPodaci.setAdresa("adresa");
        osobniPodaci.setDržava("Hrvatska");
        osobniPodaci.setPoštanskibroj(53296);
        osobniPodaci.setPrivatni(false);

        userDto.setOsobniPodaci(osobniPodaci);
        userDto.setPassword("marko_i");

        mvc.perform(post("/api/register").accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(userDto)))
                .andExpect(status().isOk());
    }

    @Test
    void saveUser_FAIL_gornjaGranica() throws Exception {
        UserDTO userDto = new UserDTO();
        Korisnik korisnik = new Korisnik();
        korisnik.setUsername("marko_l");
        korisnik.setPassword("marko_i");
        userDto.setKorisnik(korisnik);

        OsobniPodaci osobniPodaci = new OsobniPodaci();
        osobniPodaci.setIme("Marko");
        osobniPodaci.setPrezime("Ilic");
        osobniPodaci.setMobitel("0985478552");
        osobniPodaci.setEmail("marko@gmail.com");
        osobniPodaci.setAdresa("adresa");
        osobniPodaci.setDržava("Hrvatska");
        osobniPodaci.setPoštanskibroj(53297);
        osobniPodaci.setPrivatni(false);

        userDto.setOsobniPodaci(osobniPodaci);
        userDto.setPassword("marko_i");

        mvc.perform(post("/api/register").accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(userDto)))
                .andExpect(content().string("DoÅ¡lo je do pogreÅ¡ke prilikom registracije"));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}