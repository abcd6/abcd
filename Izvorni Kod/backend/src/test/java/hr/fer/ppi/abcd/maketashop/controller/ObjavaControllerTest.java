package hr.fer.ppi.abcd.maketashop.controller;

import hr.fer.ppi.abcd.maketashop.service.SpringSecurityWebAuxTestConfig;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = SpringSecurityWebAuxTestConfig.class
)
@AutoConfigureMockMvc
class ObjavaControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    @WithUserDetails("anon")
    void getAllObjava() throws Exception {
        mvc.perform(get("/api/objava")).andExpect(status().is2xxSuccessful());
    }

    @Test
    @WithUserDetails("anon")
    void getObjava() throws Exception {
        mvc.perform(get("/api/objava?id={id}", 1L)).andExpect(status().is2xxSuccessful());
    }

    @Test
    @WithUserDetails("admin")
    void deleteObjava() throws Exception {
        mvc.perform(delete("/api/obrisiObjavu?id={id}", 1L).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    @WithUserDetails("admin")
    void deleteObjava_400() throws Exception {
        mvc.perform(delete("/api/obrisiObjavu?id={id}", 20L).accept(MediaType.APPLICATION_JSON))
                .andExpect(content().string("DoÅ¡lo je do pogreÅ¡ke prilikom brisanja objave"));
    }

}