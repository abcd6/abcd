package hr.fer.ppi.abcd.maketashop.controller;

import hr.fer.ppi.abcd.maketashop.bean.KorisnikResponse;
import hr.fer.ppi.abcd.maketashop.bean.OsobniPodaciResponse;
import hr.fer.ppi.abcd.maketashop.security.MaketaShopException;
import hr.fer.ppi.abcd.maketashop.service.KorisnikService;
import hr.fer.ppi.abcd.maketashop.service.OsobniPodaciService;
import hr.fer.ppi.abcd.maketashop.utils.ValidatorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;

@RestController
public class KorisnikController {

    @Autowired
    KorisnikService korisnikService;

    @Autowired
    OsobniPodaciService osobniPodaciService;

    @GetMapping("/api/korisnik")
    public ResponseEntity<?> getKorisnik() {

        KorisnikResponse response = new KorisnikResponse();
        try {
            response = korisnikService.findAll();
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom dohvata popisa korisnika", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/api/statusKorisnik")
    public ResponseEntity<?> statusKorisnik(Long id) {
        ValidatorUtil.validateObjects(id);

        OsobniPodaciResponse response = new OsobniPodaciResponse();
        try {
            response = korisnikService.statusKorisnika(id) ? osobniPodaciService.findAll() : null;
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom dohvata statusa korinika", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ExceptionHandler({MaketaShopException.class})
    public ResponseEntity<?> handleAuthenticationException(MaketaShopException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }

}
