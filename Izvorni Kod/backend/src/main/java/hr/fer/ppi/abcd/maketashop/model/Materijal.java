package hr.fer.ppi.abcd.maketashop.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Getter
@Setter
public class Materijal {

    @JsonIgnore
    @OneToMany(mappedBy = "materijal")
    Set<MaketaMaterijal> maketaMaterijal;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String naziv;
    private Double cijena;
    private LocalDate datumkreiranja;
    private LocalDate datummodificiranja;
}
