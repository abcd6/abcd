package hr.fer.ppi.abcd.maketashop.controller;

import hr.fer.ppi.abcd.maketashop.bean.TransakcijaRequest;
import hr.fer.ppi.abcd.maketashop.bean.TransakcijaResponse;
import hr.fer.ppi.abcd.maketashop.security.MaketaShopException;
import hr.fer.ppi.abcd.maketashop.service.TransakcijaService;
import hr.fer.ppi.abcd.maketashop.utils.ValidatorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
public class TransakcijaController {

    @Autowired
    TransakcijaService transakcijaService;

    @GetMapping("/api/transakcije")
    public ResponseEntity<?> getTransakcije() {

        TransakcijaResponse response = new TransakcijaResponse();
        try {
            response = transakcijaService.findAll();
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom dohvata popisa transakcija", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/api/transakcijeKorisnik")
    public ResponseEntity<?> getTransakcijeKorisnik(Long id) {
        ValidatorUtil.validateObjects(id);

        TransakcijaResponse response = new TransakcijaResponse();
        try {
            response = transakcijaService.findAllKorisnik(id);
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom dohvata korisničkih transakcija", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @PostMapping("/api/transakcije")
    public ResponseEntity<?> postTransakcije(@RequestBody TransakcijaRequest transakcija) {
        ValidatorUtil.validateObjects(transakcija);
        ValidatorUtil.validateObjects(transakcija.getKorisnikID(), transakcija.getMaketaID(),
                transakcija.getMaterijalID(), transakcija.getCijena());
        ValidatorUtil.authorize(transakcija.getKorisnikID(), SecurityContextHolder.getContext());

        try {
            transakcijaService.save(transakcija);
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom dodavanja transakcije", e);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ExceptionHandler({MaketaShopException.class})
    public ResponseEntity<?> handleAuthenticationException(MaketaShopException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }

}