package hr.fer.ppi.abcd.maketashop.service;

import hr.fer.ppi.abcd.maketashop.bean.OsobniPodaciDeleteRequest;
import hr.fer.ppi.abcd.maketashop.bean.OsobniPodaciEditRequest;
import hr.fer.ppi.abcd.maketashop.bean.OsobniPodaciResponse;
import hr.fer.ppi.abcd.maketashop.model.OsobniPodaci;

public interface OsobniPodaciService {
    OsobniPodaciResponse findAll();

    OsobniPodaci findById(Long id);

    OsobniPodaci findByUsername(String username);

    OsobniPodaciResponse findOsobniPodaciByUsername(String username);

    boolean editOsobniPodaci(OsobniPodaciEditRequest osobniPodaci);

    boolean delete(OsobniPodaciDeleteRequest osobniPodaciDeleteRequest);

    OsobniPodaciResponse findJavniOsobniPodaci();
}
