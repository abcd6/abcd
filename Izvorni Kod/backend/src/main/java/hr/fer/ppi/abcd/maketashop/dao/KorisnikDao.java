package hr.fer.ppi.abcd.maketashop.dao;

import hr.fer.ppi.abcd.maketashop.model.Korisnik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KorisnikDao extends JpaRepository<Korisnik, Long> {

    @Query(value = "SELECT * FROM Korisnik WHERE username = :username", nativeQuery = true)
    List<Korisnik> findByUsername(@Param("username") String username);
}
