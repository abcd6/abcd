package hr.fer.ppi.abcd.maketashop.service;

import hr.fer.ppi.abcd.maketashop.bean.ObjavaEditRequest;
import hr.fer.ppi.abcd.maketashop.bean.ObjavaRequest;
import hr.fer.ppi.abcd.maketashop.bean.ObjavaResponse;

public interface ObjavaService {

    ObjavaResponse findAll();

    boolean save(ObjavaRequest novaObjava, boolean odobreno);

    boolean delete(Long id);

    boolean edit(ObjavaEditRequest objavaEditRequest);

    boolean odobriObjavu(Long id);

}
