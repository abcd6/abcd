package hr.fer.ppi.abcd.maketashop.security;

public class MaketaShopException extends RuntimeException {

    private static final long serialVersionUID = -1314420794831229345L;

    public MaketaShopException(String message, Throwable cause) {
        super(message, cause);
    }

    public MaketaShopException(String message) {
        super(message);
    }
}
