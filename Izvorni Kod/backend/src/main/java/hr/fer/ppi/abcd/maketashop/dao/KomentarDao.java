package hr.fer.ppi.abcd.maketashop.dao;

import hr.fer.ppi.abcd.maketashop.model.Komentar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface KomentarDao extends JpaRepository<Komentar, Long> {
    @Transactional
    @Modifying
    @Query(value = "DELETE FROM Komentar WHERE objavaID = :objavaID")
    void deleteKomentariByObjavaID(@Param("objavaID") Long objavaID);

    @Query(value = "SELECT * FROM Komentar WHERE objavaID = :objavaID", nativeQuery = true)
    List<Komentar> findKomentariByObjava(@Param("objavaID") Long objavaID);

    @Query(value = "SELECT * FROM Komentar WHERE korisnikID = :korisnikID", nativeQuery = true)
    List<Komentar> findByKorisnikID(@Param("korisnikID") Long id);
}
