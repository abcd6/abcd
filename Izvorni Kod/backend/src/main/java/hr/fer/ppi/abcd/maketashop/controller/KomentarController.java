package hr.fer.ppi.abcd.maketashop.controller;

import hr.fer.ppi.abcd.maketashop.bean.KomentarRequest;
import hr.fer.ppi.abcd.maketashop.bean.KomentarResponse;
import hr.fer.ppi.abcd.maketashop.security.MaketaShopException;
import hr.fer.ppi.abcd.maketashop.service.KomentarService;
import hr.fer.ppi.abcd.maketashop.utils.ValidatorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
public class KomentarController {
    @Autowired
    KomentarService komentarService;

    @GetMapping("/api/komentar")
    public ResponseEntity<?> getKomentar() {

        KomentarResponse response = new KomentarResponse();
        try {
            response = komentarService.findAll();
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom dohvata popisa komentara", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/api/komentariByObjava")
    public ResponseEntity<?> getKomentari(Long objavaID) {
        ValidatorUtil.validateObjects(objavaID);
        KomentarResponse response = new KomentarResponse();
        try {
            response.setKomentar(komentarService.findByObjavaId(objavaID));
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom dohvata komentara", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/api/komentar")
    public ResponseEntity<?> saveKomentar(@RequestBody KomentarRequest komentar) {
        ValidatorUtil.validateObjects(komentar);
        ValidatorUtil.validateObjects(komentar.getKorisnikID(), komentar.getObjavaID());
        ValidatorUtil.validateStrings(komentar.getTekst());
        ValidatorUtil.authorize(komentar.getKorisnikID(), SecurityContextHolder.getContext());
        try {
            komentarService.saveKomentar(komentar);
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom dodavanja komentara", e);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ExceptionHandler({MaketaShopException.class})
    public ResponseEntity<?> handleAuthenticationException(MaketaShopException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }
}
