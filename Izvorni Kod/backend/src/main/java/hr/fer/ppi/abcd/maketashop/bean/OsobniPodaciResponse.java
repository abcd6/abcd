package hr.fer.ppi.abcd.maketashop.bean;

import hr.fer.ppi.abcd.maketashop.model.OsobniPodaci;
import lombok.Data;

import java.util.LinkedList;
import java.util.List;

@Data
public class OsobniPodaciResponse {
    private List<OsobniPodaci> osobniPodaci;

    public OsobniPodaciResponse() {

    }

    public OsobniPodaciResponse(List<OsobniPodaci> osobniPodaci) {
        this.osobniPodaci = osobniPodaci;
    }

    public OsobniPodaciResponse(OsobniPodaci osobniPodaci) {
        this.osobniPodaci = new LinkedList<>();
        this.osobniPodaci.add(osobniPodaci);
    }
}
