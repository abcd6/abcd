package hr.fer.ppi.abcd.maketashop.bean;

import hr.fer.ppi.abcd.maketashop.model.Materijal;
import lombok.Data;

import java.util.List;

@Data
public class MaterijalResponse {

    private List<Materijal> materijal;

    public MaterijalResponse() {

    }

    public MaterijalResponse(List<Materijal> materijal) {
        this.materijal = materijal;
    }
}
