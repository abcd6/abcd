package hr.fer.ppi.abcd.maketashop.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Data
public class OsobniPodaci {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String ime;
    private String prezime;
    private LocalDate datumrođenja;
    private String mobitel;
    private String email;
    private String adresa;
    private String država;
    private Integer poštanskibroj;
    private Boolean privatni;
    private LocalDate datumkreiranja;
    private LocalDate datummodificiranja;

    @JsonIgnore
    @OneToMany(mappedBy = "osobniPodaci")
    private Set<Narudžba> narudžbe;

    @OneToOne(mappedBy = "osobniPodaci")
    private Korisnik korisnik;

}
