package hr.fer.ppi.abcd.maketashop.service.impl;

import hr.fer.ppi.abcd.maketashop.bean.KomentarRequest;
import hr.fer.ppi.abcd.maketashop.bean.KomentarResponse;
import hr.fer.ppi.abcd.maketashop.dao.KomentarDao;
import hr.fer.ppi.abcd.maketashop.model.Komentar;
import hr.fer.ppi.abcd.maketashop.model.Korisnik;
import hr.fer.ppi.abcd.maketashop.model.Objava;
import hr.fer.ppi.abcd.maketashop.service.KomentarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class KomentarServiceImpl implements KomentarService {

    @Autowired
    private KomentarDao komentarDao;

    @Override
    public KomentarResponse findAll() {
        return (new KomentarResponse(komentarDao.findAll()));
    }

    @Override
    public List<Komentar> findByObjavaId(Long id) {

        List<Komentar> tempKomentar = komentarDao.findKomentariByObjava(id);
        if (tempKomentar != null) {
            return tempKomentar;
        }

        return null;
    }

    @Override
    public boolean saveKomentar(KomentarRequest noviKomentar) {

        Komentar komentar = new Komentar();

        Korisnik tempKorisnik = new Korisnik();
        tempKorisnik.setId(noviKomentar.getKorisnikID());

        Objava tempObjava = new Objava();
        tempObjava.setId(noviKomentar.getObjavaID());

        komentar.setKorisnik(tempKorisnik);
        komentar.setObjava(tempObjava);
        komentar.setTekst(noviKomentar.getTekst());
        komentar.setDatumkreiranja(LocalDate.now());
        komentar.setDatummodificiranja(LocalDate.now());

        komentarDao.save(komentar);

        return true;
    }

}
