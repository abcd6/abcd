package hr.fer.ppi.abcd.maketashop.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Getter
@Setter
public class Maketa {

    @OneToMany(mappedBy = "maketa")
    Set<MaketaMaterijal> maketaMaterijali;
    @JsonIgnore
    @OneToMany(mappedBy = "maketa")
    Set<Transakcija> transakcije;
    @JsonIgnore
    @OneToMany(mappedBy = "maketa")
    Set<Narudžba> narudžbe;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String naziv;
    private String opis;
    private Double visina;
    private Double širina;
    private Double dužina;
    private String mjerilo;
    private String boja;
    private Double cijena;
    private Boolean standardna;
    @Lob
    private String slika;
    private LocalDate datumkreiranja;
    private LocalDate datummodificiranja;
}
