package hr.fer.ppi.abcd.maketashop.controller;

import hr.fer.ppi.abcd.maketashop.bean.MaketaRequest;
import hr.fer.ppi.abcd.maketashop.bean.MaketaResponse;
import hr.fer.ppi.abcd.maketashop.model.Maketa;
import hr.fer.ppi.abcd.maketashop.security.MaketaShopException;
import hr.fer.ppi.abcd.maketashop.service.MaketaService;
import hr.fer.ppi.abcd.maketashop.utils.ValidatorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class MaketaController {

    @Autowired
    MaketaService maketaService;

    @GetMapping("/api/maketaList")
    public ResponseEntity<?> getMaketaList() {

        List<MaketaResponse> response = new ArrayList<>();
        try {
            response = maketaService.findAllStandard();
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom dohvata popisa maketa", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/api/addMaketa")
    public ResponseEntity<?> saveMaketa(@RequestBody MaketaRequest maketaRequest) {
        // return maketaService.saveMaketa(maketaRequest) ? new
        // ResponseEntity<>(maketaService.findAll(), HttpStatus.OK) : new
        // ResponseEntity<>(HttpStatus.BAD_REQUEST);
        ValidatorUtil.validateObjects(maketaRequest);
        ValidatorUtil.validateObjects(maketaRequest.getMaketa(), maketaRequest.getMaterijalList());
        ValidatorUtil.validateStrings(maketaRequest.getMaketa().getNaziv(), maketaRequest.getMaketa().getOpis(),
                maketaRequest.getMaketa().getMjerilo(), maketaRequest.getMaketa().getBoja());
        ValidatorUtil.validateObjects(maketaRequest.getMaketa().getVisina(), maketaRequest.getMaketa().getŠirina(),
                maketaRequest.getMaketa().getDužina(), maketaRequest.getMaketa().getCijena(),
                maketaRequest.getMaketa().getStandardna());
        maketaRequest.getMaterijalList().forEach(materijal -> {
            ValidatorUtil.validateObjects(materijal);
            ValidatorUtil.validateStrings(materijal.getNaziv());
            ValidatorUtil.validateObjects(materijal.getCijena());
        });

        List<MaketaResponse> response = new ArrayList<>();
        try {
            response = maketaService.saveMaketa(maketaRequest);
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom dodavanja nove makete", e);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @GetMapping("/api/getMaketaById")
    public ResponseEntity<?> getMaketaById(Long id) {
        ValidatorUtil.validateObjects(id);
        Maketa response = new Maketa();

        try {
            response = maketaService.findById(id);
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom dohvata makete", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ExceptionHandler({MaketaShopException.class})
    public ResponseEntity<?> handleAuthenticationException(MaketaShopException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }

}
