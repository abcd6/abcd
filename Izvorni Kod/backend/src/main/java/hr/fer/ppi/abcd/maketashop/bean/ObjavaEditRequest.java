package hr.fer.ppi.abcd.maketashop.bean;

import lombok.Data;

@Data
public class ObjavaEditRequest {
    private Long objavaID;
    private String tekst;
    private String slika;
    private String video;
    private Long korisnikID;
}
