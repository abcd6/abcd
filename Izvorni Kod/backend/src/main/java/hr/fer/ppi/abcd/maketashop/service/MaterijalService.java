package hr.fer.ppi.abcd.maketashop.service;

import hr.fer.ppi.abcd.maketashop.bean.MaterijalResponse;
import hr.fer.ppi.abcd.maketashop.model.Materijal;

public interface MaterijalService {
    MaterijalResponse findAll();

    MaterijalResponse findByMaketaId(Long id);

    Materijal findById(Long materijalID);
}


