package hr.fer.ppi.abcd.maketashop.bean;

import lombok.Data;

@Data
public class NarudzbaGetRequest {
    private Long narudzbaId;
    private Double cijena;
}
