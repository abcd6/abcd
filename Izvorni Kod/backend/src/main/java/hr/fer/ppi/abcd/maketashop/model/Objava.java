package hr.fer.ppi.abcd.maketashop.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Data
public class Objava {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String naslov;
    private String tekst;

    //odlučiti hoće li biti putanja ili blob, base64
    @Lob
    private String slika;
    //odlučiti hoće li biti putanja ili blob, base64
    @Lob
    private String video;
    private Boolean odobreno;

    @JsonIgnore
    @OneToMany(mappedBy = "objava")
    private Set<Komentar> komentari;

    @ManyToOne
    @JoinColumn(name = "korisnikID")
    private Korisnik korisnik;

    private LocalDate datumkreiranja;
    private LocalDate datummodificiranja;
}
