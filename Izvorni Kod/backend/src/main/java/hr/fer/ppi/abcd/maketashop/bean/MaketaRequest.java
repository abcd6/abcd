package hr.fer.ppi.abcd.maketashop.bean;

import hr.fer.ppi.abcd.maketashop.model.Maketa;
import hr.fer.ppi.abcd.maketashop.model.Materijal;
import lombok.Data;

import java.util.List;

@Data
public class MaketaRequest {
    private Maketa maketa;
    private List<Materijal> materijalList;
}
