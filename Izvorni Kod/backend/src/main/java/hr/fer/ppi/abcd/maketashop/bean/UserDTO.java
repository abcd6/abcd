package hr.fer.ppi.abcd.maketashop.bean;

import hr.fer.ppi.abcd.maketashop.model.Korisnik;
import hr.fer.ppi.abcd.maketashop.model.OsobniPodaci;
import lombok.Data;

@Data
public class UserDTO {
    private Korisnik korisnik;
    private OsobniPodaci osobniPodaci;
    private String password;
}
