package hr.fer.ppi.abcd.maketashop.service.impl;

import hr.fer.ppi.abcd.maketashop.bean.OsobniPodaciDeleteRequest;
import hr.fer.ppi.abcd.maketashop.bean.OsobniPodaciEditRequest;
import hr.fer.ppi.abcd.maketashop.bean.OsobniPodaciResponse;
import hr.fer.ppi.abcd.maketashop.dao.*;
import hr.fer.ppi.abcd.maketashop.model.*;
import hr.fer.ppi.abcd.maketashop.service.OsobniPodaciService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class OsobniPodaciserviceImpl implements OsobniPodaciService {

    @Autowired
    private OsobniPodaciDao osobniPodaciDao;

    @Autowired
    private KorisnikDao korisnikDao;

    @Autowired
    private KomentarDao komentarDao;

    @Autowired
    private NarudzbaDao narudzbaDao;

    @Autowired
    private ObjavaDao objavaDao;

    @Autowired
    private TransakcijaDao transakcijaDao;

    @Override
    public OsobniPodaciResponse findAll() {
        return (new OsobniPodaciResponse(osobniPodaciDao.findAll()));
    }

    @Override
    public OsobniPodaciResponse findOsobniPodaciByUsername(String username) {
        return (new OsobniPodaciResponse(osobniPodaciDao.findById(korisnikDao.findByUsername(username).get(0).getId()).get()));
    }

    @Override
    public OsobniPodaci findByUsername(String username) {
        List<Korisnik> tempKorisnik = korisnikDao.findByUsername(username);
        Optional<OsobniPodaci> tempOsobniPodaci = osobniPodaciDao.findById(tempKorisnik.get(0).getOsobniPodaci().getId());
        return tempOsobniPodaci.get();
    }

    @Override
    public OsobniPodaci findById(Long id) {
        Optional<Korisnik> tempKorisnik = korisnikDao.findById(id);
        Optional<OsobniPodaci> tempOsobniPodaci = osobniPodaciDao.findById(tempKorisnik.get().getOsobniPodaci().getId());
        return tempOsobniPodaci.get();
    }

    @Override
    public boolean editOsobniPodaci(OsobniPodaciEditRequest osobniPodaciEditRequest) {
        Optional<Korisnik> tempKorisnik = korisnikDao.findById(osobniPodaciEditRequest.getId());
        Optional<OsobniPodaci> tempOsobniPodaci = osobniPodaciDao.findById(tempKorisnik.get().getOsobniPodaci().getId());

        if (!tempOsobniPodaci.isPresent()) {
            return false;
        }

        OsobniPodaci op = new OsobniPodaci();

        op.setId(tempOsobniPodaci.get().getId());
        op.setIme(Objects.isNull(osobniPodaciEditRequest.getIme()) ? tempOsobniPodaci.get().getIme() : osobniPodaciEditRequest.getIme());
        op.setPrezime(Objects.isNull(osobniPodaciEditRequest.getPrezime()) ? tempOsobniPodaci.get().getPrezime() : osobniPodaciEditRequest.getPrezime());
        op.setDatumrođenja(Objects.isNull(osobniPodaciEditRequest.getDatumrođenja()) ? tempOsobniPodaci.get().getDatumrođenja() : osobniPodaciEditRequest.getDatumrođenja());
        op.setMobitel(Objects.isNull(osobniPodaciEditRequest.getMobitel()) ? tempOsobniPodaci.get().getMobitel() : osobniPodaciEditRequest.getMobitel());
        op.setEmail(Objects.isNull(osobniPodaciEditRequest.getEmail()) ? tempOsobniPodaci.get().getEmail() : osobniPodaciEditRequest.getEmail());
        op.setAdresa(Objects.isNull(osobniPodaciEditRequest.getAdresa()) ? tempOsobniPodaci.get().getAdresa() : osobniPodaciEditRequest.getAdresa());
        op.setDržava(Objects.isNull(osobniPodaciEditRequest.getDržava()) ? tempOsobniPodaci.get().getDržava() : osobniPodaciEditRequest.getDržava());
        op.setPoštanskibroj(Objects.isNull(osobniPodaciEditRequest.getPoštanskibroj()) ? tempOsobniPodaci.get().getPoštanskibroj() : osobniPodaciEditRequest.getPoštanskibroj());
        op.setPrivatni(tempOsobniPodaci.get().getPrivatni());
        op.setDatumkreiranja(tempOsobniPodaci.get().getDatumkreiranja());
        op.setDatummodificiranja(LocalDate.now());

        this.osobniPodaciDao.save(op);

        return true;

    }

    @Override
    public boolean delete(OsobniPodaciDeleteRequest osobniPodaciDeleteRequest) {

        Optional<OsobniPodaci> tempOsobniPodaci = osobniPodaciDao.findById(osobniPodaciDeleteRequest.getId());

        if (!tempOsobniPodaci.isPresent()) {
            return false;
        }

        // treba implementirati kaskadno brisanje

        osobniPodaciDao.deleteById(osobniPodaciDeleteRequest.getId());

        List<Komentar> lista_kom = komentarDao.findByKorisnikID(osobniPodaciDeleteRequest.getId());
        for (Komentar k : lista_kom) {
            komentarDao.deleteById(k.getId());
        }

        List<Narudžba> lista_nar = narudzbaDao.findByOsobniPodaciID(osobniPodaciDeleteRequest.getId());
        for (Narudžba n : lista_nar) {
            narudzbaDao.deleteById(n.getId());
        }

        List<Objava> lista_obj = objavaDao.findByKorisnikid(osobniPodaciDeleteRequest.getId());
        for (Objava o : lista_obj) {
            objavaDao.deleteById(o.getId());
        }

        List<Transakcija> lista_tr = transakcijaDao.findByKorisnikId(osobniPodaciDeleteRequest.getId());
        for (Transakcija t : lista_tr) {
            transakcijaDao.deleteById(t.getId());
        }

        korisnikDao.deleteById(osobniPodaciDeleteRequest.getId());

        return true;
    }

    @Override
    public OsobniPodaciResponse findJavniOsobniPodaci() {

        return (new OsobniPodaciResponse(osobniPodaciDao.findJavniOsobniPodaci()));
    }
}
