package hr.fer.ppi.abcd.maketashop.dao;

import hr.fer.ppi.abcd.maketashop.model.Maketa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MaketaDao extends JpaRepository<Maketa, Long> {
    @Query(value = "SELECT * FROM Maketa WHERE Standardna = TRUE", nativeQuery = true)
    List<Maketa> findAllStandard();
}
