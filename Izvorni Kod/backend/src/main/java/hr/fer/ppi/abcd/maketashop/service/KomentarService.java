package hr.fer.ppi.abcd.maketashop.service;

import hr.fer.ppi.abcd.maketashop.bean.KomentarRequest;
import hr.fer.ppi.abcd.maketashop.bean.KomentarResponse;
import hr.fer.ppi.abcd.maketashop.model.Komentar;

import java.util.List;

public interface KomentarService {
    KomentarResponse findAll();

    List<Komentar> findByObjavaId(Long id);

    boolean saveKomentar(KomentarRequest request);
}
