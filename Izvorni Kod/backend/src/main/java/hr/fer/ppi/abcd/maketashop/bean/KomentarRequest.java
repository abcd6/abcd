package hr.fer.ppi.abcd.maketashop.bean;

import lombok.Data;

@Data
public class KomentarRequest {
    private Long korisnikID;
    private Long objavaID;
    private String tekst;
}
