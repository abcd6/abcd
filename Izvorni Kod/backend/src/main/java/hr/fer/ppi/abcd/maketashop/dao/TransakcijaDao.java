package hr.fer.ppi.abcd.maketashop.dao;

import hr.fer.ppi.abcd.maketashop.model.Transakcija;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransakcijaDao extends JpaRepository<Transakcija, Long> {
    @Query(value = "SELECT * FROM Transakcija WHERE korisnikID = :korisnikID", nativeQuery = true)
    List<Transakcija> findByKorisnikId(@Param("korisnikID") Long id);
}
