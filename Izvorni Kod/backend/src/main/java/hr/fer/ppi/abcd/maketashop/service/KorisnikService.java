package hr.fer.ppi.abcd.maketashop.service;

import hr.fer.ppi.abcd.maketashop.bean.KorisnikResponse;

public interface KorisnikService {
    KorisnikResponse findAll();

    boolean statusKorisnika(Long id);
}
