package hr.fer.ppi.abcd.maketashop.controller;

import hr.fer.ppi.abcd.maketashop.bean.MaketaResponse;
import hr.fer.ppi.abcd.maketashop.bean.NarudzbaGetRequest;
import hr.fer.ppi.abcd.maketashop.bean.NarudzbaRequest;
import hr.fer.ppi.abcd.maketashop.bean.NarudzbaResponse;
import hr.fer.ppi.abcd.maketashop.security.JwtUserDetails;
import hr.fer.ppi.abcd.maketashop.security.MaketaShopException;
import hr.fer.ppi.abcd.maketashop.service.MaketaService;
import hr.fer.ppi.abcd.maketashop.service.NarudzbaService;
import hr.fer.ppi.abcd.maketashop.utils.ValidatorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.ArrayList;
import java.util.List;

@RestController
public class NarudzbaController {

    @Autowired
    NarudzbaService narudzbaService;

    @Autowired
    MaketaService maketaService;

    @GetMapping("/api/narudzbe")
    public ResponseEntity<?> getNarudzbe() {

        NarudzbaResponse response = new NarudzbaResponse();
        try {
            response = narudzbaService.findAll();
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom dohvata popisa narudžbi", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @GetMapping("/api/narudzbeKorisnik")
    public ResponseEntity<?> getNarudzbeKorisnik(Long id) {
        ValidatorUtil.validateObjects(id);
        ValidatorUtil.authorize(id, SecurityContextHolder.getContext());

        NarudzbaResponse response = new NarudzbaResponse();
        try {
            response = narudzbaService.findAllKorisnik(id);
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom dohvata korisničkih narudžbi", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @PostMapping("/api/novaNarudzba")
    public ResponseEntity<?> postNarudzba(@RequestBody NarudzbaRequest narudzba) {
        ValidatorUtil.validateObjects(narudzba);
        ValidatorUtil.validateObjects(narudzba.getOsobniPodaciId(), narudzba.getMaketa(), narudzba.getMaterijalId());
        ValidatorUtil.validateStrings(narudzba.getMaketa().getNaziv(), narudzba.getMaketa().getOpis(),
                narudzba.getMaketa().getMjerilo(), narudzba.getMaketa().getBoja());
        ValidatorUtil.validateObjects(narudzba.getMaketa().getVisina(), narudzba.getMaketa().getŠirina(),
                narudzba.getMaketa().getDužina(), narudzba.getMaketa().getStandardna());
        List<MaketaResponse> response = new ArrayList<>();
        try {
            narudzbaService.save(narudzba);
            response = maketaService.findAllStandard();
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom dodavanja narudžbe", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/api/odobriNarudzbuAdmin")
    public ResponseEntity<?> odobriNarudzbuAdmin(@RequestBody NarudzbaGetRequest request) {

        NarudzbaResponse response = new NarudzbaResponse();
        try {
            narudzbaService.odobriNarudzbuAdminISpremiCijenu(request.getNarudzbaId(), request.getCijena());
            response = narudzbaService.findAll();
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom odobravanja narudžbe", e);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/api/odobriNarudzbuKorisnik")
    public ResponseEntity<?> odobriNarudzbuKorisnik(Long id) {
        ValidatorUtil.validateObjects(id);

        NarudzbaResponse response = new NarudzbaResponse();
        try {
            narudzbaService.odobriNarudzbuKorisnik(id, ((JwtUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
            response = narudzbaService.findAllKorisnik(((JwtUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom prihvaćanja narudžbe", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/api/obrisiNarudzbu")
    public ResponseEntity<?> obrisiNarudzbu(Long id){
        ValidatorUtil.validateObjects(id);
        NarudzbaResponse response = new NarudzbaResponse();

        try {
            narudzbaService.obrisiNarudzbu(id, ((JwtUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
            response = narudzbaService.findAllKorisnik(((JwtUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom brisanja narudžbe", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ExceptionHandler({MaketaShopException.class})
    public ResponseEntity<?> handleAuthenticationException(MaketaShopException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }

}
