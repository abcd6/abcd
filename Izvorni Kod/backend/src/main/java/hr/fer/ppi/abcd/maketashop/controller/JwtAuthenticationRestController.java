package hr.fer.ppi.abcd.maketashop.controller;

import hr.fer.ppi.abcd.maketashop.bean.JwtTokenRequest;
import hr.fer.ppi.abcd.maketashop.bean.JwtTokenResponse;
import hr.fer.ppi.abcd.maketashop.bean.UserDTO;
import hr.fer.ppi.abcd.maketashop.security.JwtUserDetails;
import hr.fer.ppi.abcd.maketashop.security.MaketaShopException;
import hr.fer.ppi.abcd.maketashop.service.impl.JwtInMemoryUserDetailsServiceImpl;
import hr.fer.ppi.abcd.maketashop.utils.JwtTokenUtil;
import hr.fer.ppi.abcd.maketashop.utils.ValidatorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@RestController
public class JwtAuthenticationRestController {

    @Value("${jwt.http.request.header}")
    private String tokenHeader;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtInMemoryUserDetailsServiceImpl jwtInMemoryUserDetailsService;

    @PostMapping("/api/login")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtTokenRequest authenticationRequest)
            throws MaketaShopException {

        ValidatorUtil.validateObjects(authenticationRequest);
        ValidatorUtil.validateObjects(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        final JwtUserDetails userDetails = jwtInMemoryUserDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());

        final String token = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new JwtTokenResponse(token));
    }

    @PostMapping("/api/register")
    public ResponseEntity<?> saveUser(@RequestBody UserDTO user) {
        ValidatorUtil.validateObjects(user);
        ValidatorUtil.validateObjects(user.getKorisnik(), user.getOsobniPodaci());
        ValidatorUtil.validateStrings(user.getKorisnik().getUsername(), user.getPassword(),
                user.getOsobniPodaci().getIme(), user.getOsobniPodaci().getPrezime(),
                user.getOsobniPodaci().getMobitel(),
                user.getOsobniPodaci().getEmail(), user.getOsobniPodaci().getAdresa(),
                user.getOsobniPodaci().getDržava(), user.getOsobniPodaci().getPoštanskibroj().toString(),
                user.getOsobniPodaci().getPrivatni().toString());
        try {
            jwtInMemoryUserDetailsService.save(user);
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom registracije");
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/api/refreshtoken")
    public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {
        String authToken = request.getHeader(tokenHeader);
        final String token = authToken.substring(7);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUserDetails user = (JwtUserDetails) jwtInMemoryUserDetailsService.loadUserByUsername(username);

        if (jwtTokenUtil.canTokenBeRefreshed(token)) {
            String refreshedToken = jwtTokenUtil.refreshToken(token);
            return ResponseEntity.ok(new JwtTokenResponse(refreshedToken));
        } else {
            throw new MaketaShopException("Došlo je do pogreške prilikom osvježavanja tokena");
        }
    }

    @ExceptionHandler({MaketaShopException.class})
    public ResponseEntity<?> handleAuthenticationException(MaketaShopException e) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
    }

    private void authenticate(String username, String password) {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new MaketaShopException("Korisnik nije aktivan", e);
        } catch (BadCredentialsException e) {
            throw new MaketaShopException("Neispravno korisničko ime ili lozinka", e);
        }
    }
}
