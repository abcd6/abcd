package hr.fer.ppi.abcd.maketashop.controller;

import hr.fer.ppi.abcd.maketashop.bean.MaterijalResponse;
import hr.fer.ppi.abcd.maketashop.model.Materijal;
import hr.fer.ppi.abcd.maketashop.security.MaketaShopException;
import hr.fer.ppi.abcd.maketashop.service.MaterijalService;
import hr.fer.ppi.abcd.maketashop.utils.ValidatorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MaterijalController {

    @Autowired
    MaterijalService materijalService;

    @GetMapping("/api/materijali")
    public ResponseEntity<?> getMaterijali() {

        MaterijalResponse response = new MaterijalResponse();
        try {
            response = materijalService.findAll();
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom dohvata popisa materijala", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    @GetMapping("/api/getMaterialListByMaketaId")
    public ResponseEntity<?> getMaterijalListByMaketayId(Long id) {
        ValidatorUtil.validateObjects(id);
        MaterijalResponse response = new MaterijalResponse();

        try {
            response = materijalService.findByMaketaId(id);
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom dohvata popisa materijala", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/api/getMaterijalById")
    public ResponseEntity<?> getMaterijalById(Long materijalID) {
        ValidatorUtil.validateObjects(materijalID);
        Materijal response = new Materijal();

        try {
            response = materijalService.findById(materijalID);
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom dohvata materijala", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ExceptionHandler({MaketaShopException.class})
    public ResponseEntity<?> handleAuthenticationException(MaketaShopException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }
}
