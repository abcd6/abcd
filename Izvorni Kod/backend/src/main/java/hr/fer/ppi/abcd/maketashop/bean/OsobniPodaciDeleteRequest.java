package hr.fer.ppi.abcd.maketashop.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class OsobniPodaciDeleteRequest {
    @JsonIgnore
    private Long id;
}
