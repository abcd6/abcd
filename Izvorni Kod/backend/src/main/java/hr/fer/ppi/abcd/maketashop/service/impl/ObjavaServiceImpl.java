package hr.fer.ppi.abcd.maketashop.service.impl;

import hr.fer.ppi.abcd.maketashop.bean.ObjavaEditRequest;
import hr.fer.ppi.abcd.maketashop.bean.ObjavaRequest;
import hr.fer.ppi.abcd.maketashop.bean.ObjavaResponse;
import hr.fer.ppi.abcd.maketashop.dao.KomentarDao;
import hr.fer.ppi.abcd.maketashop.dao.KorisnikDao;
import hr.fer.ppi.abcd.maketashop.dao.ObjavaDao;
import hr.fer.ppi.abcd.maketashop.model.Korisnik;
import hr.fer.ppi.abcd.maketashop.model.Objava;
import hr.fer.ppi.abcd.maketashop.security.MaketaShopException;
import hr.fer.ppi.abcd.maketashop.service.ObjavaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Optional;

@Service
public class ObjavaServiceImpl implements ObjavaService {

    @Autowired
    private ObjavaDao objavaDao;

    @Autowired
    private KomentarDao komentarDao;

    @Autowired
    private KorisnikDao korisnikDao;

    @Override
    public ObjavaResponse findAll() {
        return (new ObjavaResponse(objavaDao.findAll()));
    }

    @Override
    public boolean save(ObjavaRequest novaObjava, boolean odobreno) {
        Objava objava = new Objava();

        Optional<Korisnik> tempKorisnik = korisnikDao.findById(novaObjava.getKorisnikID());

        objava.setNaslov(novaObjava.getNaslov());
        objava.setTekst(novaObjava.getTekst());
        objava.setSlika(novaObjava.getSlika());
        objava.setVideo(novaObjava.getVideo());
        objava.setOdobreno(odobreno);
        objava.setKorisnik(tempKorisnik.get());

        objava.setDatumkreiranja(LocalDate.now());
        objava.setDatummodificiranja(LocalDate.now());

        objavaDao.save(objava);

        return true;
    }

    @Override
    public boolean delete(Long id) {

        Optional<Objava> tempObjava = objavaDao.findById(id);

        if (!tempObjava.isPresent()) {
            throw new MaketaShopException("Maketa nije pronađena");
        }

        komentarDao.deleteKomentariByObjavaID(id);
        objavaDao.deleteById(id);

        return true;
    }

    @Override
    public boolean edit(ObjavaEditRequest objavaEditRequest) {

        Optional<Objava> tempObjava = objavaDao.findById(objavaEditRequest.getObjavaID());

        if (!tempObjava.isPresent()) {
            return false;
        }

        if (objavaEditRequest.getKorisnikID().equals(1) || tempObjava.get().getKorisnik().getId().equals(objavaEditRequest.getKorisnikID())) {
            Objava objava = new Objava();

            Korisnik tempKorisnik = new Korisnik();
            tempKorisnik.setId(objavaEditRequest.getKorisnikID());

            objava.setId(Objects.isNull(objavaEditRequest.getObjavaID()) ? tempObjava.get().getId() : objavaEditRequest.getObjavaID());
            objava.setTekst(Objects.isNull(objavaEditRequest.getTekst()) ? tempObjava.get().getTekst() : objavaEditRequest.getTekst());
            //objava.setSlika(Objects.isNull(objavaEditRequest.getSlika()) ? tempObjava.get().getSlika() : objavaEditRequest.getSlika());
            objava.setVideo(Objects.isNull(objavaEditRequest.getVideo()) ? tempObjava.get().getVideo() : objavaEditRequest.getVideo());
            objava.setOdobreno(tempObjava.get().getOdobreno());
            objava.setKorisnik(tempKorisnik);

            objava.setDatumkreiranja(tempObjava.get().getDatumkreiranja());
            objava.setDatummodificiranja(LocalDate.now());

            objavaDao.save(objava);
            return true;
        }

        return false;
    }

    @Override
    public boolean odobriObjavu(Long id) {
        Optional<Objava> tempObjava = objavaDao.findById(id);

        if (!tempObjava.isPresent()) {
            return false;
        }

        Objava objava = tempObjava.get();
        objava.setOdobreno(true);
        objavaDao.save(objava);

        return true;
    }
}
