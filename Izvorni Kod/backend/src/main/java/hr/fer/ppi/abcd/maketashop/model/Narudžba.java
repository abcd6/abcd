package hr.fer.ppi.abcd.maketashop.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
public class Narudžba {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "osobniPodaciID")
    private OsobniPodaci osobniPodaci;

    @ManyToOne
    @JoinColumn(name = "maketaID")
    private Maketa maketa;

    @ManyToOne
    @JoinColumn(name = "materijalID")
    private Materijal materijal;

    private Integer stanje;
    private Double cijena;
    private LocalDate datumkreiranja;
    private LocalDate datummodificiranja;
}
