package hr.fer.ppi.abcd.maketashop.service;

import hr.fer.ppi.abcd.maketashop.bean.NarudzbaRequest;
import hr.fer.ppi.abcd.maketashop.bean.NarudzbaResponse;

public interface NarudzbaService {
    NarudzbaResponse findAll();

    NarudzbaResponse findAllKorisnik(Long id);

    boolean save(NarudzbaRequest novaNarudzba);

    boolean odobriNarudzbuAdminISpremiCijenu(Long id, Double cijena);

    boolean odobriNarudzbuKorisnik(Long id, Long korisnikID);

    void obrisiNarudzbu(Long id, Long korisnikID);
}
