package hr.fer.ppi.abcd.maketashop.bean;

import hr.fer.ppi.abcd.maketashop.model.Transakcija;
import lombok.Data;

import java.util.List;

@Data
public class TransakcijaResponse {
    private List<Transakcija> transakcija;

    public TransakcijaResponse() {

    }

    public TransakcijaResponse(List<Transakcija> transakcija) {
        this.transakcija = transakcija;
    }
}
