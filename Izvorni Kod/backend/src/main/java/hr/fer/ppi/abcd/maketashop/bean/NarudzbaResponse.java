package hr.fer.ppi.abcd.maketashop.bean;

import hr.fer.ppi.abcd.maketashop.model.Narudžba;
import lombok.Data;

import java.util.List;

@Data
public class NarudzbaResponse {

    private List<Narudžba> narudzba;

    public NarudzbaResponse() {

    }

    public NarudzbaResponse(List<Narudžba> narudzba) {
        this.narudzba = narudzba;
    }
}
