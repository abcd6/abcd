package hr.fer.ppi.abcd.maketashop.dao;

import hr.fer.ppi.abcd.maketashop.model.OsobniPodaci;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OsobniPodaciDao extends JpaRepository<OsobniPodaci, Long> {
    @Query(value = "SELECT * FROM Osobni_podaci WHERE privatni = FALSE", nativeQuery = true)
    List<OsobniPodaci> findJavniOsobniPodaci();
}
