package hr.fer.ppi.abcd.maketashop.controller;

import hr.fer.ppi.abcd.maketashop.bean.ObjavaEditRequest;
import hr.fer.ppi.abcd.maketashop.bean.ObjavaRequest;
import hr.fer.ppi.abcd.maketashop.bean.ObjavaResponse;
import hr.fer.ppi.abcd.maketashop.security.JwtUserDetails;
import hr.fer.ppi.abcd.maketashop.security.MaketaShopException;
import hr.fer.ppi.abcd.maketashop.service.ObjavaService;
import hr.fer.ppi.abcd.maketashop.utils.ValidatorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
public class ObjavaController {

    @Autowired
    ObjavaService objavaService;

    @GetMapping("/api/objava")
    public ResponseEntity<?> getObjave() {

        ObjavaResponse response = new ObjavaResponse();
        try {
            response = objavaService.findAll();
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom dohvata popisa objava", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);

    }


    @PostMapping("/api/objava")
    public ResponseEntity<?> postObjave(@RequestBody ObjavaRequest objava) {
        ValidatorUtil.validateObjects(objava);
        ValidatorUtil.validateObjects(objava.getKorisnikID());
        ValidatorUtil.validateStrings(objava.getNaslov(), objava.getTekst());
        ValidatorUtil.authorize(objava.getKorisnikID(), SecurityContextHolder.getContext());
        ObjavaResponse response = new ObjavaResponse();
        try {
            boolean odobreno = false;
            if (((JwtUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId().equals(1L)) {
                odobreno = true;
            }
            objavaService.save(objava, odobreno);
            response = objavaService.findAll();
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom dodavanja objave", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("/api/obrisiObjavu")
    public ResponseEntity<?> obrisiObjavu(Long id) {
        ValidatorUtil.validateObjects(id);
        ObjavaResponse response = new ObjavaResponse();
        try {
            objavaService.delete(id);
            response = objavaService.findAll();
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom brisanja objave", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("/api/objava")
    public ResponseEntity<?> editObjava(@RequestBody ObjavaEditRequest objava) {
        ValidatorUtil.validateObjects(objava);
        ValidatorUtil.validateObjects(objava.getObjavaID(), objava.getKorisnikID());
        ValidatorUtil.authorize(objava.getKorisnikID(), SecurityContextHolder.getContext());
        try {
            objavaService.edit(objava);
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom uređivanja objave", e);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/api/odobriObjavu")
    public ResponseEntity<?> odobriObjavu(Long id) {

        ObjavaResponse response = new ObjavaResponse();
        try {
            objavaService.odobriObjavu(id);
            response = objavaService.findAll();
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom odobravanja objave", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @ExceptionHandler({MaketaShopException.class})
    public ResponseEntity<?> handleAuthenticationException(MaketaShopException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }
}
