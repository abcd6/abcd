package hr.fer.ppi.abcd.maketashop.bean;

import lombok.Data;

@Data
public class TransakcijaRequest {
    private Long korisnikID;
    private Long maketaID;
    private Long materijalID;
    private Double cijena;
}