package hr.fer.ppi.abcd.maketashop.service;

import hr.fer.ppi.abcd.maketashop.bean.TransakcijaRequest;
import hr.fer.ppi.abcd.maketashop.bean.TransakcijaResponse;

public interface TransakcijaService {

    TransakcijaResponse findAll();

    TransakcijaResponse findAllKorisnik(Long id);

    boolean save(TransakcijaRequest novaTransakcija);
}
