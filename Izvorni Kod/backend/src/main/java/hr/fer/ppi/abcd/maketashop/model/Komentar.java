package hr.fer.ppi.abcd.maketashop.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
public class Komentar {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //vidjeti za anon usere
    @ManyToOne
    @JoinColumn(name = "korisnikID")
    private Korisnik korisnik;

    @ManyToOne
    @JoinColumn(name = "objavaID")
    private Objava objava;

    private String tekst;
    private LocalDate datumkreiranja;
    private LocalDate datummodificiranja;
}
