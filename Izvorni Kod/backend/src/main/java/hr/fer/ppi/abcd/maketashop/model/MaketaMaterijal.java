package hr.fer.ppi.abcd.maketashop.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class MaketaMaterijal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "maketaID")
    private Maketa maketa;

    @ManyToOne
    @JoinColumn(name = "materijalID")
    private Materijal materijal;
}
