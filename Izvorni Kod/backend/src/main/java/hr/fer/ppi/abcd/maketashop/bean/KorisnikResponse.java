package hr.fer.ppi.abcd.maketashop.bean;

import hr.fer.ppi.abcd.maketashop.model.Korisnik;
import lombok.Data;

import java.util.List;

@Data
public class KorisnikResponse {

    private List<Korisnik> korisnik;

    public KorisnikResponse() {
    }

    public KorisnikResponse(List<Korisnik> korisnik) {
        this.korisnik = korisnik;
    }
}