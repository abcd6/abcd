package hr.fer.ppi.abcd.maketashop.bean;

import hr.fer.ppi.abcd.maketashop.model.Komentar;
import lombok.Data;

import java.util.List;

@Data
public class KomentarResponse {
    private List<Komentar> komentar;

    public KomentarResponse() {

    }

    public KomentarResponse(List<Komentar> komentar) {
        this.komentar = komentar;
    }


}

