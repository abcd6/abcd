package hr.fer.ppi.abcd.maketashop.bean;

import hr.fer.ppi.abcd.maketashop.model.Objava;
import lombok.Data;

import java.util.List;

@Data
public class ObjavaResponse {

    private List<Objava> objava;

    public ObjavaResponse() {

    }

    public ObjavaResponse(List<Objava> objava) {
        this.objava = objava;
    }
}
