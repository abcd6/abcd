package hr.fer.ppi.abcd.maketashop.dao;

import hr.fer.ppi.abcd.maketashop.model.Narudžba;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NarudzbaDao extends JpaRepository<Narudžba, Long> {

    @Query(value = "SELECT * FROM Narudžba WHERE osobni_PodaciID = :osobniPodaciID", nativeQuery = true)
    List<Narudžba> findByOsobniPodaciID(@Param("osobniPodaciID") Long id);
}
