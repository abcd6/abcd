package hr.fer.ppi.abcd.maketashop.service.impl;

import hr.fer.ppi.abcd.maketashop.bean.UserDTO;
import hr.fer.ppi.abcd.maketashop.dao.KorisnikDao;
import hr.fer.ppi.abcd.maketashop.dao.OsobniPodaciDao;
import hr.fer.ppi.abcd.maketashop.model.Korisnik;
import hr.fer.ppi.abcd.maketashop.model.OsobniPodaci;
import hr.fer.ppi.abcd.maketashop.security.JwtUserDetails;
import hr.fer.ppi.abcd.maketashop.security.MaketaShopException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class JwtInMemoryUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private PasswordEncoder bcryptEncoder;
    @Autowired
    private KorisnikDao userDao;
    @Autowired
    private OsobniPodaciDao osobniPodaciDao;

    public boolean save(UserDTO user) {
        if (user.getOsobniPodaci().getPoštanskibroj() < 10000 ||
                user.getOsobniPodaci().getPoštanskibroj() > 53296) {
            throw new MaketaShopException("Poštanski broj nije u intervalu");
        }


        OsobniPodaci newData = user.getOsobniPodaci();
        Korisnik newUser = new Korisnik();

        newData.setDatumkreiranja(LocalDate.now());
        newData.setDatummodificiranja(LocalDate.now());
        OsobniPodaci osobniPodaci = osobniPodaciDao.save(newData);

        newUser.setUsername(user.getKorisnik().getUsername());
        newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
        newUser.setRola("ROLE_USER");
        newUser.setAktivan(true);
        newUser.setOsobniPodaci(osobniPodaci);

        Korisnik isOkUser = userDao.save(newUser);
        return isOkUser != null ? true : false;
    }

    @Override
    public JwtUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        List<Korisnik> user = userDao.findByUsername(username);
        if (user.get(0) == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        if (user.get(0).getAktivan() == false) {
            throw new UsernameNotFoundException("User is blocked");
        }

        JwtUserDetails tempJwtUserDetails = new JwtUserDetails(user.get(0));

        return tempJwtUserDetails;
    }

}
