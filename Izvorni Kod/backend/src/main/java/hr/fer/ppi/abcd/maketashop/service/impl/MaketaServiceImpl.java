package hr.fer.ppi.abcd.maketashop.service.impl;

import hr.fer.ppi.abcd.maketashop.bean.MaketaRequest;
import hr.fer.ppi.abcd.maketashop.bean.MaketaResponse;
import hr.fer.ppi.abcd.maketashop.dao.MaketaDao;
import hr.fer.ppi.abcd.maketashop.dao.MaketaMaterijalDao;
import hr.fer.ppi.abcd.maketashop.model.Maketa;
import hr.fer.ppi.abcd.maketashop.model.MaketaMaterijal;
import hr.fer.ppi.abcd.maketashop.model.Materijal;
import hr.fer.ppi.abcd.maketashop.service.MaketaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MaketaServiceImpl implements MaketaService {

    @Autowired
    private MaketaDao maketaDAO;

    @Autowired
    private MaketaMaterijalDao maketaMaterijalDao;

    @Override
    public List<MaketaResponse> findAll() {
        List<Maketa> tempMaketaList = maketaDAO.findAll();
        List<MaketaResponse> tempMap = new ArrayList<>();
        for (Maketa tempMaketa : tempMaketaList) {
            List<MaketaMaterijal> tempMaketaMaterijalList = maketaMaterijalDao.findByMaketaId(tempMaketa.getId());
            List<Materijal> tempMaterijalList = new ArrayList<>();
            for (MaketaMaterijal tempMaketaMaterijal : tempMaketaMaterijalList) {
                tempMaterijalList.add(tempMaketaMaterijal.getMaterijal());
            }
            tempMap.add(new MaketaResponse(tempMaketa, tempMaterijalList));
        }

        return tempMap;
    }


    @Override
    public List<MaketaResponse> findAllStandard() {

        List<Maketa> tempMaketaList = maketaDAO.findAllStandard();
        List<MaketaResponse> tempMap = new ArrayList<>();
        for (Maketa tempMaketa : tempMaketaList) {
            List<MaketaMaterijal> tempMaketaMaterijalList = maketaMaterijalDao.findByMaketaId(tempMaketa.getId());
            List<Materijal> tempMaterijalList = new ArrayList<>();
            for (MaketaMaterijal tempMaketaMaterijal : tempMaketaMaterijalList) {
                tempMaterijalList.add(tempMaketaMaterijal.getMaterijal());
            }
            tempMap.add(new MaketaResponse(tempMaketa, tempMaterijalList));
        }

        return tempMap;
    }

    @Override
    public List<MaketaResponse> saveMaketa(MaketaRequest request) {
        request.getMaketa().setDatumkreiranja(LocalDate.now());
        request.getMaketa().setDatummodificiranja(LocalDate.now());
        Maketa tempMaketa = maketaDAO.saveAndFlush(request.getMaketa());

        //materijali save
        for (Materijal materijal : request.getMaterijalList()) {
            MaketaMaterijal tempMaketaMaterijal = new MaketaMaterijal();
            tempMaketaMaterijal.setMaketa(tempMaketa);
            tempMaketaMaterijal.setMaterijal(materijal);
            maketaMaterijalDao.saveAndFlush(tempMaketaMaterijal);
        }

        return findAllStandard();
    }

    @Override
    public Maketa findById(Long id) {
        Optional<Maketa> tempMaketa = maketaDAO.findById(id);
        if (tempMaketa.isPresent()) {
            return tempMaketa.get();
        }

        return null;
    }


}
