package hr.fer.ppi.abcd.maketashop.dao;

import hr.fer.ppi.abcd.maketashop.model.MaketaMaterijal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MaketaMaterijalDao extends JpaRepository<MaketaMaterijal, Long> {

    @Query(value = "SELECT * FROM Maketa_Materijal WHERE maketaID = :id", nativeQuery = true)
    List<MaketaMaterijal> findByMaketaId(@Param("id") Long id);
}
