package hr.fer.ppi.abcd.maketashop.service;

import hr.fer.ppi.abcd.maketashop.bean.MaketaRequest;
import hr.fer.ppi.abcd.maketashop.bean.MaketaResponse;
import hr.fer.ppi.abcd.maketashop.model.Maketa;

import java.util.List;

public interface MaketaService {
    List<MaketaResponse> findAll();

    List<MaketaResponse> saveMaketa(MaketaRequest request);

    Maketa findById(Long id);

    List<MaketaResponse> findAllStandard();

}
