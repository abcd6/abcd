package hr.fer.ppi.abcd.maketashop.service.impl;

import hr.fer.ppi.abcd.maketashop.bean.MaterijalResponse;
import hr.fer.ppi.abcd.maketashop.dao.MaketaMaterijalDao;
import hr.fer.ppi.abcd.maketashop.dao.MaterijalDao;
import hr.fer.ppi.abcd.maketashop.model.MaketaMaterijal;
import hr.fer.ppi.abcd.maketashop.model.Materijal;
import hr.fer.ppi.abcd.maketashop.service.MaterijalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class MaterijalServiceImpl implements MaterijalService {

    @Autowired
    private MaterijalDao materijalDao;

    @Autowired
    private MaketaMaterijalDao maketaMaterijalDao;

    @Override
    public MaterijalResponse findAll() {
        return (new MaterijalResponse(materijalDao.findAll()));
    }

    @Override
    public MaterijalResponse findByMaketaId(Long id) {
        List<MaketaMaterijal> maketaMaterijal = maketaMaterijalDao.findAll();
        List<Materijal> response = new LinkedList<>();
        for (MaketaMaterijal m : maketaMaterijal) {
            if (m.getMaketa().getId().equals(id)) {
                response.add(m.getMaterijal());
            }
        }
        return (new MaterijalResponse(response));
    }

    @Override
    public Materijal findById(Long materijalID) {
        Optional<Materijal> tempMaterijal = materijalDao.findById(materijalID);
        if (tempMaterijal.isPresent()) {
            return tempMaterijal.get();
        }

        return null;
    }

    ;

}
