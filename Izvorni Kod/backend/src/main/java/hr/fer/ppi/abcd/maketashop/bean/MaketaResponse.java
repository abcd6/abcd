package hr.fer.ppi.abcd.maketashop.bean;

import hr.fer.ppi.abcd.maketashop.model.Maketa;
import hr.fer.ppi.abcd.maketashop.model.Materijal;
import lombok.Data;

import java.util.List;

@Data
public class MaketaResponse {

    private Maketa maketa;
    private List<Materijal> materijalList;

    public MaketaResponse() {

    }

    public MaketaResponse(Maketa maketa, List<Materijal> materijalList) {
        this.maketa = maketa;
        this.materijalList = materijalList;
    }
}
