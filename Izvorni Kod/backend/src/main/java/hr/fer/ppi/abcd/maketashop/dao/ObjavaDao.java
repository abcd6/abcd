package hr.fer.ppi.abcd.maketashop.dao;

import hr.fer.ppi.abcd.maketashop.model.Objava;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ObjavaDao extends JpaRepository<Objava, Long> {
    @Query(value = "SELECT * FROM Objava WHERE korisnikID = :korisnikID", nativeQuery = true)
    List<Objava> findByKorisnikid(@Param("korisnikID") Long id);
}
