package hr.fer.ppi.abcd.maketashop.service.impl;

import hr.fer.ppi.abcd.maketashop.bean.KorisnikResponse;
import hr.fer.ppi.abcd.maketashop.dao.KorisnikDao;
import hr.fer.ppi.abcd.maketashop.model.Korisnik;
import hr.fer.ppi.abcd.maketashop.service.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class KorisnikServiceImpl implements KorisnikService {

    @Autowired
    private KorisnikDao korisnikDao;

    @Override
    public KorisnikResponse findAll() {
        return (new KorisnikResponse(korisnikDao.findAll()));
    }

    @Override
    public boolean statusKorisnika(Long id) {
        Optional<Korisnik> tempKorisnik = korisnikDao.findById(id);

        if (!tempKorisnik.isPresent()) {
            return false;
        }

        Korisnik korisnik = tempKorisnik.get();
        korisnik.setAktivan(!korisnik.getAktivan());
        korisnikDao.save(korisnik);

        return true;
    }

}
