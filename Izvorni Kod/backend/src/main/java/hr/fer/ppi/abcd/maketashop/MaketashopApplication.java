package hr.fer.ppi.abcd.maketashop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaketashopApplication {

    public static void main(String[] args) {
        SpringApplication.run(MaketashopApplication.class, args);
    }

}
