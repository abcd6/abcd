package hr.fer.ppi.abcd.maketashop.service.impl;

import hr.fer.ppi.abcd.maketashop.bean.TransakcijaRequest;
import hr.fer.ppi.abcd.maketashop.bean.TransakcijaResponse;
import hr.fer.ppi.abcd.maketashop.dao.TransakcijaDao;
import hr.fer.ppi.abcd.maketashop.model.Korisnik;
import hr.fer.ppi.abcd.maketashop.model.Maketa;
import hr.fer.ppi.abcd.maketashop.model.Materijal;
import hr.fer.ppi.abcd.maketashop.model.Transakcija;
import hr.fer.ppi.abcd.maketashop.service.TransakcijaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class TransakcijaServiceImpl implements TransakcijaService {

    @Autowired
    private TransakcijaDao transakcijaDao;

    @Override
    public TransakcijaResponse findAll() {
        return (new TransakcijaResponse(transakcijaDao.findAll()));
    }

    @Override
    public boolean save(TransakcijaRequest novaTransakcija) {

        Transakcija transakcija = new Transakcija();

        Korisnik tempKorisnik = new Korisnik();
        tempKorisnik.setId(novaTransakcija.getKorisnikID());

        Maketa tempMaketa = new Maketa();
        tempMaketa.setId(novaTransakcija.getMaketaID());

        Materijal tempMaterijal = new Materijal();
        tempMaterijal.setId(novaTransakcija.getMaterijalID());

        transakcija.setCijena(novaTransakcija.getCijena());

        transakcija.setDatumkreiranja(LocalDate.now());
        transakcija.setDatummodificiranja(LocalDate.now());

        transakcija.setKorisnik(tempKorisnik);
        transakcija.setMaketa(tempMaketa);
        transakcija.setMaterijal(tempMaterijal);


        transakcijaDao.save(transakcija);

        return true;
    }

    @Override
    public TransakcijaResponse findAllKorisnik(Long id) {

        List<Transakcija> tempTransakcija = transakcijaDao.findByKorisnikId(id);

        if (tempTransakcija != null) {
            return (new TransakcijaResponse(tempTransakcija));
        }

        return null;

    }

}
