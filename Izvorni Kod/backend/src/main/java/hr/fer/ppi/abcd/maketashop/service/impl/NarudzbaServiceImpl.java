package hr.fer.ppi.abcd.maketashop.service.impl;

import hr.fer.ppi.abcd.maketashop.bean.NarudzbaRequest;
import hr.fer.ppi.abcd.maketashop.bean.NarudzbaResponse;
import hr.fer.ppi.abcd.maketashop.dao.*;
import hr.fer.ppi.abcd.maketashop.model.*;
import hr.fer.ppi.abcd.maketashop.security.MaketaShopException;
import hr.fer.ppi.abcd.maketashop.service.NarudzbaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class NarudzbaServiceImpl implements NarudzbaService {

    @Autowired
    private NarudzbaDao narudzbaDao;

    @Autowired
    private MaketaDao maketaDao;

    @Autowired
    private MaketaMaterijalDao maketaMaterijalDao;

    @Autowired
    private MaterijalDao materijalDao;

    @Autowired
    private TransakcijaDao transakcijaDao;

    @Autowired
    private KorisnikDao korisnikDao;

    @Override
    public NarudzbaResponse findAll() {
        return (new NarudzbaResponse(narudzbaDao.findAll()));
    }

    //Sta tocno napravit sa osobnim podacima, maketom i id-om
    @Override
    public boolean save(NarudzbaRequest novaNarudzba) {
        novaNarudzba.getMaketa().setDatumkreiranja(LocalDate.now());
        novaNarudzba.getMaketa().setDatummodificiranja(LocalDate.now());
        Maketa maketa = maketaDao.save(novaNarudzba.getMaketa());

        Optional<Materijal> m = materijalDao.findById(novaNarudzba.getMaterijalId());

        //materijali save

        MaketaMaterijal tempMaketaMaterijal = new MaketaMaterijal();
        tempMaketaMaterijal.setMaketa(maketa);
        tempMaketaMaterijal.setMaterijal(m.get());
        maketaMaterijalDao.saveAndFlush(tempMaketaMaterijal);


        Narudžba narudzba = new Narudžba();

        OsobniPodaci osobniPodaci = new OsobniPodaci();
        osobniPodaci.setId(novaNarudzba.getOsobniPodaciId());

        narudzba.setStanje(0);
        narudzba.setMaketa(maketa);
        narudzba.setOsobniPodaci(osobniPodaci);
        narudzba.setDatumkreiranja(LocalDate.now());
        narudzba.setDatummodificiranja(LocalDate.now());
        narudzba.setCijena(maketa.getCijena());
        narudzba.setMaterijal(m.get());

        narudzbaDao.save(narudzba);

        return true;
    }


    @Override
    public boolean odobriNarudzbuAdminISpremiCijenu(Long id, Double cijena) {
        Optional<Narudžba> tempNarudzba = narudzbaDao.findById(id);

        if (!tempNarudzba.isPresent()) {
            return false;
        }

        Narudžba narudzba = tempNarudzba.get();
        narudzba.setStanje(1);
        narudzba.setCijena(cijena);
        narudzbaDao.save(narudzba);

        return true;
    }

    @Override
    public boolean odobriNarudzbuKorisnik(Long id, Long korisnikID) {
        Optional<Narudžba> tempNarudzba = narudzbaDao.findById(id);

        if (!tempNarudzba.isPresent() || !tempNarudzba.get().getOsobniPodaci().getKorisnik().getId().equals(korisnikID)) {
            throw new MaketaShopException("Pogreška prihvaćanja narudžbe.");
        }

        Narudžba narudzba = tempNarudzba.get();
        narudzba.setStanje(2);
        narudzbaDao.save(narudzba);

        //transakcija
        Transakcija transakcija = new Transakcija();

        Korisnik tempKorisnik = new Korisnik();
        tempKorisnik.setId(tempNarudzba.get().getOsobniPodaci().getKorisnik().getId());

        Maketa tempMaketa = new Maketa();
        tempMaketa.setId((tempNarudzba.get().getMaketa().getId()));

        Materijal tempMaterijal = new Materijal();
        tempMaterijal.setId(tempNarudzba.get().getMaterijal().getId());

        transakcija.setKorisnik(tempKorisnik);
        transakcija.setMaketa(tempMaketa);
        transakcija.setMaterijal(tempMaterijal);
        transakcija.setCijena(tempNarudzba.get().getCijena());
        transakcija.setDatumkreiranja(LocalDate.now());
        transakcija.setDatummodificiranja(LocalDate.now());

        transakcijaDao.save(transakcija);

        return true;
    }

    @Override
    public void obrisiNarudzbu(Long id, Long korisnikID) {
        Optional<Narudžba> tempNarudzba = narudzbaDao.findById(id);

        if (!tempNarudzba.isPresent() || (!tempNarudzba.get().getOsobniPodaci().getKorisnik().getId().equals(korisnikID) && !korisnikID.equals(1L))) {
            throw new MaketaShopException("Greška prilikom brisanja narudžbe.");
        }

        narudzbaDao.deleteById(id);

        List<MaketaMaterijal> tempMaketaMaterijal = maketaMaterijalDao.findByMaketaId(tempNarudzba.get().getMaketa().getId());
        maketaMaterijalDao.deleteInBatch(tempMaketaMaterijal);

        maketaDao.deleteById(tempNarudzba.get().getMaketa().getId());
    }

    @Override
    public NarudzbaResponse findAllKorisnik(Long id) {

        List<Narudžba> tempNarudzba = new ArrayList<>();

        if(id.equals(1L)){
            tempNarudzba = narudzbaDao.findAll();
        }else{
            Optional<Korisnik> korisnk = korisnikDao.findById(id);
            tempNarudzba = narudzbaDao.findByOsobniPodaciID(korisnk.get().getOsobniPodaci().getId());
        }

        return (new NarudzbaResponse(tempNarudzba));
    }

}
