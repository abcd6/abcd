package hr.fer.ppi.abcd.maketashop.bean;

import lombok.Data;

@Data
public class ObjavaDeleteRequest {
    private Long objavaID;
    private Long korisnikID;
}
