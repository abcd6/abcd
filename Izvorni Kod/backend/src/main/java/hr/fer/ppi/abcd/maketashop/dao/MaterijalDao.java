package hr.fer.ppi.abcd.maketashop.dao;

import hr.fer.ppi.abcd.maketashop.model.Materijal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MaterijalDao extends JpaRepository<Materijal, Long> {
}
