package hr.fer.ppi.abcd.maketashop.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
public class Transakcija {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "korisnikID")
    private Korisnik korisnik;

    @ManyToOne
    @JoinColumn(name = "maketaID")
    private Maketa maketa;

    @ManyToOne
    @JoinColumn(name = "materijalID")
    private Materijal materijal;

    private Double cijena;
    private LocalDate datumkreiranja;
    private LocalDate datummodificiranja;
}
