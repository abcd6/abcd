package hr.fer.ppi.abcd.maketashop.bean;

import lombok.Data;

import java.time.LocalDate;

@Data
public class OsobniPodaciEditRequest {
    private Long id;
    private String ime;
    private String prezime;
    private LocalDate datumrođenja;
    private String mobitel;
    private String email;
    private String adresa;
    private String država;
    private Integer poštanskibroj;
}
