package hr.fer.ppi.abcd.maketashop.utils;

import hr.fer.ppi.abcd.maketashop.security.JwtUserDetails;
import hr.fer.ppi.abcd.maketashop.security.MaketaShopException;
import org.springframework.security.core.context.SecurityContext;

public class ValidatorUtil {

    public static void validateStrings(String... strings) {
        for (String string : strings) {
            if (string == null || "".equals(string.trim())) {
                throw new MaketaShopException("Zahtjev nije ispravno popunjen");
            }
        }
    }

    public static void validateObjects(Object... objects) {
        for (Object object : objects) {
            if (object == null) {
                throw new MaketaShopException("Zahtjev nije ispravno popunjen");
            }
        }
    }

    public static void authorize(Long korisnikID, SecurityContext context) {

        if (context.getAuthentication() != null) {
            if (!korisnikID.equals(((JwtUserDetails) context.getAuthentication().getPrincipal()).getId())) {
                throw new MaketaShopException("Zahtjev nije ispravno popunjen");
            }
        } else {
            if (!korisnikID.equals(Long.valueOf(2))) {
                throw new MaketaShopException("Zahtjev nije ispravno popunjen");
            }
        }
    }
}
