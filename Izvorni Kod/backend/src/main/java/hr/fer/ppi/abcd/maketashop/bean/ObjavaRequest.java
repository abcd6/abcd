package hr.fer.ppi.abcd.maketashop.bean;

import lombok.Data;

import javax.persistence.Lob;

@Data
public class ObjavaRequest {
    private String naslov;
    private String tekst;
    @Lob
    private String slika;
    @Lob
    private String video;
    private Long korisnikID;
}
