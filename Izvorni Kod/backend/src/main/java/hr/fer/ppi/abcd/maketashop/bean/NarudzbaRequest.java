package hr.fer.ppi.abcd.maketashop.bean;

import hr.fer.ppi.abcd.maketashop.model.Maketa;
import lombok.Data;

@Data
public class NarudzbaRequest {

    private Long osobniPodaciId;
    private Maketa maketa;
    private Long materijalId;
}
