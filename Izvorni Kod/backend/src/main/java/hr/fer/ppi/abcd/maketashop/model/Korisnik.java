package hr.fer.ppi.abcd.maketashop.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
public class Korisnik {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String username;

    @JsonIgnore
    @Column
    private String password;

    @Column
    private String rola;

    @Column
    private Boolean aktivan;

    @JsonIgnore
    @OneToMany(mappedBy = "korisnik")
    private Set<Objava> objave;

    @JsonIgnore
    @OneToMany(mappedBy = "korisnik")
    private Set<Komentar> komentari;

    @JsonIgnore
    @OneToMany(mappedBy = "korisnik")
    private Set<Transakcija> transakcije;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "osobniPodaciID", referencedColumnName = "id")
    private OsobniPodaci osobniPodaci;
}
