package hr.fer.ppi.abcd.maketashop.controller;

import hr.fer.ppi.abcd.maketashop.bean.OsobniPodaciDeleteRequest;
import hr.fer.ppi.abcd.maketashop.bean.OsobniPodaciEditRequest;
import hr.fer.ppi.abcd.maketashop.bean.OsobniPodaciResponse;
import hr.fer.ppi.abcd.maketashop.model.OsobniPodaci;
import hr.fer.ppi.abcd.maketashop.security.MaketaShopException;
import hr.fer.ppi.abcd.maketashop.service.OsobniPodaciService;
import hr.fer.ppi.abcd.maketashop.utils.ValidatorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class OsobniPodaciController {

    @Autowired
    OsobniPodaciService osobniPodaciService;

    @GetMapping("/api/osobniPodaciAdmin")
    public ResponseEntity<?> getOsobniPodaci() {
        OsobniPodaciResponse response = new OsobniPodaciResponse();
        try {
            response = osobniPodaciService.findAll();
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom dohvata popisa osobnih podataka", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("api/javniOsobniPodaci")
    public ResponseEntity<?> getjavniOsobniPodaci() {
        OsobniPodaciResponse response = new OsobniPodaciResponse();
        try {
            response = osobniPodaciService.findJavniOsobniPodaci();
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom dohvata popisa osobnih podataka", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/api/osobniPodaciKorisnik")
    public ResponseEntity<?> getOsobniPodaciKorisnik(String username) {
        ValidatorUtil.validateStrings(username);

        OsobniPodaciResponse response = new OsobniPodaciResponse();
        try {
            response = osobniPodaciService.findOsobniPodaciByUsername(username);
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom dohvata osobnih podataka", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/api/getOsobniPodaciByUsername")
    public ResponseEntity<?> getOsobniPodaciByUsername(String username) {
        ValidatorUtil.validateStrings(username);
        OsobniPodaci response = new OsobniPodaci();
        try {
            response = osobniPodaciService.findByUsername(username);
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom dohvata osobnih podataka", e);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/api/osobniPodaci")
    public ResponseEntity<?> editOsobniPodaci(@RequestBody OsobniPodaciEditRequest osobniPodaci) {
        ValidatorUtil.validateObjects(osobniPodaci);
        ValidatorUtil.validateObjects(osobniPodaci.getId());
        OsobniPodaci response = new OsobniPodaci();
        try {
            osobniPodaciService.editOsobniPodaci(osobniPodaci);
            response = osobniPodaciService.findById(osobniPodaci.getId());
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom uređivanja osobnih podataka", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/api/getOsobniPodaciById")
    public ResponseEntity<?> getOsobniPodaciById(Long id) {
        ValidatorUtil.validateObjects(id);
        OsobniPodaci response = new OsobniPodaci();

        try {
            response = osobniPodaciService.findById(id);
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom dohvata osobnih podataka", e);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/api/osobniPodaci")
    public ResponseEntity<?> deleteOsobniPodaci(OsobniPodaciDeleteRequest osobniPodaci) {
        ValidatorUtil.validateObjects(osobniPodaci);
        ValidatorUtil.validateObjects(osobniPodaci.getId());
        try {
            osobniPodaciService.delete(osobniPodaci);
        } catch (Exception e) {
            throw new MaketaShopException("Došlo je do pogreške prilikom brisanja osobnih podataka", e);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ExceptionHandler({MaketaShopException.class})
    public ResponseEntity<?> handleAuthenticationException(MaketaShopException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }

}
